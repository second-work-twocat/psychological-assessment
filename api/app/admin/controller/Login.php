<?php

namespace app\admin\controller;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use app\model\Login as ModelLogin;
use app\model\User as ModelUser;
use config\upload as upload;
use app\model\Token as ModelToken;
use app\admin\controller\Token as CToken;

class Login extends BaseController
{
     /**
      * 登录验证
      * @author L
      *todo 判断用户登录是否正确
      */
     public function Login(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'User|账号' => 'require',
               'PassWord|密码' => 'require',
          ]);

          //!账号和密码要一样
          $con[] = ['login_user', '=', $params['User']];
          $con[] = ['login_password', '=', $params['PassWord']];
          // 登录验证信息
          $Login_verify = ModelLogin::where($con)->find();
          if (!$Login_verify) {
               $rtn = Tool::msg(404, "登录失败!");
               return $rtn;
          }
   
          // 获取关联
          $Login_Profile = ModelLogin::where($con)->find()->userRola->toArray();
          // 获取登录
          $Login = ModelLogin::where($con)->select()->append(["token"])->push($Login_Profile)->toArray();
          // !拿取Token
          $token = ModelToken::where("token_user",$params["User"])->find();
          if (!$token) {
               // 创造Token
               // echo "asd";
               $create = (new CToken())->CreateToken($params["User"]);
               $Login[0]["token"] = $create;
          }else{
               $Login[0]["token"] = $token["token_value"];
          }
       

          $rtn = Tool::Message(200, "登录成功!", $Login);
          return  $rtn;
     }
     /**
      * 获取用户登录信息
      * todo 登录成功后，获取用户的相关信息
      */
     public function getUser(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'guid|用户guid' => 'require',
               'login_user|账号' => 'require',
               'login_password|密码' => 'require',
          ]);
          $con[] = ['login_user', '=', $params['login_user']];
          $con[] = ['login_password', '=', $params['login_password']];
          // *获取关联表User信息
          $User = ModelUser::where('login_guid', $params['guid'])->find();
          // // *获取Login的信息
          ////$Login = ModelLogin::where($con)->select()->push($User)->toArray();
          $Login = Tool::HasOne(ModelLogin::class, 'rola', $con, [], 'login_rola_guid', 'rola_guid')->push($User)->toArray();
          return Tool::msg(200, "获取成功!", $Login);
     }
     /**
      * 编辑用户
      * todo 编辑用户信息
      */
     public function EditUser(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'guid|账号guid' => 'require',
               'User|账号' => 'require|max:10',
               'Password|密码' => 'require|max:10',
               'Information|用户个人信息' => 'require',
               'Img|头像' => 'require',
          ]);
          $change_user = [
               'login_user' => $params['User'],
               'login_password' => $params['Password']
          ];
          // *编辑账号和密码
          $EditUser = ModelLogin::where('login_guid', $params['guid'])->find()->save($change_user);
          // 编辑信息
          $change_information = [
               'user_information' => $params['Information'],

          ];
          //编辑头像
          $change_heard = [
               'user_img' => $params['Img'],
          ];
          $EditUser = ModelUser::where('login_guid', $params['guid'])->find()->save($change_information);
          $EditUser = ModelUser::where('login_guid', $params['guid'])->find()->save($change_heard);
          return Tool::msg(200, "编辑成功!");
     }
     /**
      * 获取账号列表
      * @author L
      * todo 这个是获取已经验证的账号
      */
     public function getLoginList()
     {
          $Select = ModelLogin::where('login_start', 1)
               ->field([
                    'login_guid',
                    'login_user',
                    'login_password',
                    'login_start',
                    'login_create_time'
               ])
               ->order('login_create_time')
               ->select()
               ->toArray();
          return Tool::msg(200, "获取成功!", $Select);
     }
     /**
      * 获取未验证账号列表
      * @author  L
      */
     public function getNotVerifiedLogin()
     {
          $Select = ModelLogin::where('login_start', 0)
               ->field([
                    'login_guid',
                    'login_user',
                    'login_password',
                    'login_start',
                    'login_create_time'
               ])
               ->order('login_create_time')
               ->select()
               ->toArray();
          return Tool::msg(200, "获取成功!", $Select);
     }
     /**
      * 新建账号
      * @author L
      * todo 新建一个账号，但是是处于未验证状态,需要根据当事人的同意才行!
      */
     public function CreateLogin(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'User|账号' => 'require|max:10',
               'Password|密码' => 'require|max:10',
          ]);
          $login_guid = Tool::AutomaticID($params['User']);
          $create = [
               'login_guid' => $login_guid,
               'login_user' => $params['User'],
               'login_password' => $params['Password'],
               'login_rola_guid' => 'ab336.66f0f8af9.f8af97ad3b985.97ad3b985db7', //*默认为新用户
          ];
          $create = ModelLogin::create($create);
          return Tool::msg(200, "新建成功!");
     }
     /**
      * 删除账号
      * @author L
      */
     public function DeleteLogin(Request $request)
     {

          $params = $request->param();
          $this->validate($params, [
               'User|账号' => 'require',
               'Password|密码' => 'require',
          ]);
          $con[] = ['login_user', '=', $params['User']];
          $con[] = ['login_password', '=', $params['Password']];
          $delete = ModelLogin::Where($con)->find()->delete();
          return Tool::msg(200, "删除成功!");
     }
     /**
      * 验证账号
      * @author  L 
      */
     public function VerifiedLogin(Request $request)
     {
          $params = $request->param();
          $this->validate(
               $params,
               [
                    'User|账号' => 'require',
                    'Password|密码' => 'require',
               ]
          );
          $guid = Tool::AutomaticID($params['User']);
          $con[] = ['login_user', '=', $params['User']];
          $con[] = ['login_password', '=', $params['Password']];
          $change = [
               'login_start' => 1,
          ];
          $verified = ModelLogin::where($con)->find()->save($change);
          //*新建一个用户
          $createUser =
               [
                    'user_guid' => $guid,
                    'user_name' => $params['User'],
                    'user_img' => '/',
                    'user_information' => '你好!',
                    'login_guid' => $guid,
               ];
          $createUser = ModelUser::create($createUser);
          return Tool::msg(200, "验证成功!");
     }
}
