<?php


namespace app\admin\controller;

use app\BaseController;
use app\Request;
use app\admin\controller\Tool as Tool;
use app\model\Weclome as ModelWeclome;

class Weclome extends BaseController
{
     /**
      * 随机获取欢迎语
      * @author L 
      */
     public function getWeclomeList()
     {
          $model = ModelWeclome::class;
          $data = [
               'weclome_content',
               'weclome_create_time',
          ];
          $Weclome = Tool::AllRandGetText($model, 'weclome_id', $data);
          return $Weclome;
     }
}
