<?php

namespace app\admin\controller;

use app\BaseController;
use think\App;
use think\exception\ValidateException;
use think\Model;
use think\Validate;
use think\db\BaseQuery;


/**
 * 服务工具类
 * @author L
 * @access public
 */
class ServiceTool extends BaseController
{



    /**
     * 参数不能为空值
     * @author L
     * @access public
     * @param string $value 不能为空的参数
     * @return 
     **/
    public static function NotNull(string $value)
    {
        if (!is_string($value))
            $data = Tool::msg(500, $value . "这不是一个有效的字符串!");
        if ($value === null or $value === "") {
            $data = Tool::msg(404, "参数不能为空!");
            return $data;
        } else {
            return $value;
        }
    }
    /**
     * 数组参数不能为空值
     * @author L
     * @access public
     * @param array $value 输入的传来的参数
     **/
    public static function ArrNotNull(array $value)
    {
        if (!is_array($value))
            $data = Tool::msg(500, $value . "这不是一个有效的数组!");
        if ($value === null or $value === "") {
            $data = Tool::msg(500, "数组参数不能为空!");
            return $data;
        } else {
            return $value;
        }
    }
    /**
     * 判断查询结果是否为空->(为空则报错)
     * @author L
     * @access public
     * @param $data 传入查询后的内容data
     * @param string $wrong 传入报错提示内容wrong
     * @param string $success 传入正确提示内容success
     */
    public static function JudgmentNull($data, string $wrong = "不能为空", string $success = "OK")
    {
        $data = Tool::NotNull($data);
        if ($data == "[]") {
            $data = Tool::msg(500, $wrong);
            return $data;
        } else {
            $data = Tool::msg(200, $success);
            return $data;
        }
    }
    /**
     * 查询是否返回值->(查询成功和查询失败)
     * @author L
     * @access public
     * @param  $data 传入判断的内容data
     * @param  string $wrong 传入失败提示内容wrong
     * @param  string $success 传入成功提示内容success
     */
    public static function JudgmentSuccess($data, $wrong = "失败!", $success = "成功!")
    {
        $data = Tool::NotNull($data);
        if ($data == 1) {
            $value = Tool::msg(200, $success);
            return $value;
        } else {
            $value = Tool::msg(500, $wrong);
            return $value;
        }
    }
    /**
     * 中英文字符->(中文字符转换英文字符)
     * @author L
     * @access public
     * @param string $character 中文字符
     */
    public static function ChineseAndEnglish(string $character)
    {
        $character = ServiceTool::NotNull($character);
        $data = [
            '，' => ',',
            '。' => '.',
            '？' => '?',
            '；' => ';',
            '‘' => "'",
            '“' => '"',
            '：' => ':',
            '【' => '[',
            '】' => ']',

        ];
        return $data[$character];
    }
    /**
     * 获取Token值(转换)->(根据当前日期的转换Token)
     * @author L
     * @access public
     * @param string $time 当前时间
     */
    public static function TokenChange(string $time)
    {
        if (!is_string($time))
            $value = Tool::msg(500, $time . "这不是一个有效的时间!");
        $time = ServiceTool::NotNull($time);
        $value = md5($time); //转md5
        $value = str_rot13($value); //转ROT13
        //分散拼接
        $first = substr($value, 0, 5);
        $second = substr($value, 10, 15);
        $value = $first . $second;
        return $value;
    }
    /**
     * 递归获取无限父子关系树
     * @author L
     * @access public
     * @param array $value 查询的值
     * @param array $model 全部模型层
     * @param array $relevancetable 关联键
     * @param int $i 初始值
     */
    public static function getRelationTree(array $value, array $model, array $relevancetable, int $i)
    {
        //*循环取值
        foreach ($value as $key => $index_value) {
            //*防止(栈)益出->拿到全部表，则return返回，结束递归
            if ($i <= 0) {
                return $value;
            }
            $data = implode($model[$i - 1])::where(implode($relevancetable[$i - 1]), $index_value[implode($relevancetable[$i])])->select()->append(['children'])->toArray();
            $data[0]['children'] = $index_value;
            return ServiceTool::getRelationTree($data, $model, $relevancetable, $i - 1); //*调用自身函数，达到递归效果
        }
    }


}