<?php
/**
 * Token验证
 */
namespace app\admin\controller;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use app\model\Token as ModelToken;
use think\facade\Cookie;

class Token extends BaseController
{
     /**
      * 创造当前时间Token值
      * @author L
      * todo 创造一条Token约期使用.
      */
     public function CreateToken($user)
     {
          $Time = Tool::MySQLDataTime(); //当前时间
          $Token_value = Tool::Salt($Time,true,"!@#$%^&*;~`/*-");
          $date = date_create($Time);
          date_add($date, date_interval_create_from_date_string("1 days")); //*约期时间(1天)
          $expire_time = date_format($date, "Y-m-d H:i:s");
          //*创建Token
          $data = [
               'token_user'=>$user,
               'token_value' => $Token_value,
               'token_expire_time' => $expire_time,
          ];
          $rtn = ModelToken::create($data);
          $rtn = ModelToken::where('token_expire_time', $expire_time)->select()->toArray();
          return $Token_value;
     }
     /**
      * 点击登录->Token验证是否存在
      * @author L
      * todo 当点击登录时，会判断是否有Token存在,如果有则继续使用，没有则创造一条
      */
     public function TokenVerify(Request $request)
     {
          $param = $request->param();
          $this->validate($param,[
               'User|Token用户'=>'require|max:20'
          ]);
          // ?判断该用户是否有Token约期存在
          $determine = ModelToken::where('token_user',$param['User'])->select()->toArray();
          
          if ($determine)//!有Token约期就继续使用
          {
               return Tool::msg(200, "Token约期中~~~",$determine);
          } else { //!没有Token约期就提示返回创造
               return Tool::msg(404,"没有Token约期!");
          }
     }
     /**
      * Token验证约期
      * @author L
      * todo 判断当前时间是否超过Token约期,超过则删除，没超过则继续使用
      */
      public function TokenVerifyTime(Request $request)
      {
          $param = $request->param();
          $this->validate($param,[
               'User|Token用户'=>'require|max:20'
          ]);
          //获取当前时间
          $Time = Tool::MySQLDataTime();
          // 获取Token约期
          $determine = ModelToken::where('token_user',$param['User'])->select()->toArray();
          if(!$determine){return Tool::msg(404,"Token已过期!");}
          if($Time >= $determine[0]['token_expire_time']){
               // $data = [
               //      "token_delete_time"=>$Time,
               // ];
               $value = ModelToken::where('token_expire_time',$determine[0]['token_expire_time'])->delete();
               $value =  Tool::msg(404,"Token已过期!"); 
               return $value;
          }else{
               return Tool::msg(200,"当前时间:{$Time}-Token到期时间为{$determine[0]['token_expire_time']}");
          }
     }
     /**
      * Token拿值判断
      * @author L
      * todo 这个一般来说是防止其他人直接跳过登录页面去操作，这是违反规则的。所以要拿取本地Token值进行和数据库的判断是否一致,如果一致则通过判断，否则退回登录页
      */
      public function TokenValue(Request $request)
      {
          $params = $request->param();
          $this->validate($params,[
               'token_value|Token值'=>'require',
          ]);
          $rtn = ModelToken::where('token_value',$params['token_value'])->select()->toArray();
         if($rtn){
          return Tool::msg(200,"Token验证完毕!",$rtn);
         }else{
          return Tool::msg(404,"Token错误!");
         }
      }
}