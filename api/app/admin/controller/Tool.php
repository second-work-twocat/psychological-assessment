<?php

/**
 * +-----------------------------------------+
 * | Tool工具类(22级封装);                                       |
 * +-----------------------------------------+
 * | QQ:2149573631;                                            |
 * +-----------------------------------------+
 */

namespace app\admin\controller;

use app\BaseController;
use think\App;
use think\exception\ValidateException;
use think\{Model, Validate};
use think\db\BaseQuery;
use app\admin\controller\ServiceTool as Service; //服务Tool
use Exception;
use think\response\Json;
use think\Container;
use think\exception\{HttpException, HttpResponseException};
use think\facade\{Cache, Db, Request, Route, Session};
use think\Response;
use think\response\Jsonp;
use think\response\Redirect;
use think\route\Url as UrlBuild;
use config\code\code as CodeConfig;
use app\model\Token as ModelToken;


define("MODELHAVEVALUE", 1); //*判断模型层是否有值

/**
 * 封装工具类[(工具)](Tool.php)
 * @access public
 * @author L
 * @file Tool.php
 * @version 6.0
 * @data 2023.6.26
 * @extends ServiceTool
 * @name Tool 工具类
 * @test 工具类里面包含很多有意思的方法(例如:基本方法、对应前端的方法、对应后端的方法、对应数据库的方法)~:)
 * !路径不对会报错(把Tool.php和ServiceTool.php工具PHP文件放到controller控制层文件夹里面)
 * todo @Vipe By L
 * ?如有调整，请自行修改!
 */
class Tool extends Service
{

    /**
     * 基本变量值->方便获取(不调用方法的情况下直接获取)
     * @author L
     * @access public
     */
    /**小皮根路径 */
    public static string $PHP_Study_Root = "C:\phpstudy_pro\WWW/";
    /**Tool工具类的类路径 */
    public static $Tool_CLASS = __CLASS__;
    /**Tool工具类的根路径 */
    public static $Tool__FILE = __FILE__;
    /**Tool工具类的命名空间 */
    public static $Tool_NAMESPACE = __NAMESPACE__;

    /**
     * Token判断器
     * @author L
     * @access public
     * @param mixed $token 请求头token值-必填
     * TODO 用来后台系统的接口身份验证。
     * @date 2023/11/09
     * @return mixed
     */
    public function TokenJudgment(mixed $token)
    {
        //?判断是否传入Token请求头
        if($token == null || $token == false)throw new HttpException(505,"请携带Token！");
        //?拿取数据库Token
        $SQLToken = Db::table("token")->where('token_value',$token)->find();
        if(!$SQLToken || $SQLToken == null)return self::msg(500,'登录超时！',int:"请确定Token是否正确！");
        return Tool::Message(200,"验证成功！",func:__FUNCTION__);
    }
    /**
     * 自动生成账号和密码（乱）
     * @author L 
     * @static
     * @access public
     * @param bool $write 是否写入表-选填
     * @param mixed $model 模型层-选填->(这里推荐用登录)
     * @param array $field 写入的字段-选填->(这里默认["login_user","login_password"])->只能支持两条
     * TODO 用来快速新建账号和密码。
     * @date 2023/10/09
     * @return array|mixed
     */
    public static function MakeUserAndPassword(bool $write = false,mixed $model = "",array $field = ["login_user","login_password"]){
        $con =[];
        $con["User"] = substr(self::Salt(self::GetNewDate("New")),1,8);
        $con["Password"] = substr(self::Salt(self::GetNewDate("Day")),1,8);
        //!排除异常
        if(count($field) != 2)return self::msg(404,"参数错误！");
        if(!$write) return $con;//*返回数据
        if($write)
        {
            $data = [
                $field[0] => $con["User"],
                $field[1] => $con["Password"],
            ];
            $end  = $model::create($data);
            return Tool::msg(200,"新建成功！");
        }
       
    }



    /**
     * 加密盐
     * @static
     * @author L
     * @access public
     * @param string $key 盐值-必填
     * @param bool $status 是否规格化-选填
     * @param string $$malization 规格化字符-选填
     * @date 2023/10/05
     * TODO 用于使用高级性且安全性强的功能使用的加密盐。
     * @return string
     */
    public static function Salt(string $key, bool $status = false, string $malization = "-"):string
    {
        //将盐md5编码
        $key_md5 = md5($key);
        //将盐sha1哈希编码
        $key_sha1 = sha1($key);
        //将盐base64_encode->base编码
        $key_base64_encode = base64_encode($key);
        //将盐urlencode url路由编码
        $key_urlencode = urlencode($key);
        if (!$status) {
            $key = substr($key_md5, 0, 100) . substr($key_base64_encode, 0, 100) . substr($key_urlencode, 0, 100) . substr($key_sha1, 0, 100);
            $key = str_rot13($key);//转换rot13
            return $key;
        }
        if($status)
        {
            $key = substr($key_md5, 0, 100) .$malization. substr($key_base64_encode, 0, 100) .$malization. substr($key_urlencode, 0, 100) .$malization. substr($key_sha1, 0, 100);
            $key = str_rot13($key);//转换rot13
            return $key;
        }
    }

    /**
     * 参数批量验证器
     * @author L
     * @access public
     * @param array $params  参数-必填
     * @param array $verification 验证条件-必填
     * @date 2023/9/13 
     * TODO 用于前端或前台POST传递参数时候的验证
     * @return mixed
     */
    public function BatchVerification(array $params, array $verification): mixed
    {
        //*获取param参数所有Key键
        $params_keys = array_keys($params);
        for ($i = 0; $i < count($verification); $i++) {
            //*防双斜杆
            $verification[$i] = str_replace("\/", "", $verification[$i]);
            //*转换数组获取参数索引
            $verification_arr = explode("/", $verification[$i]);
            $Verification_key = in_array($verification_arr[0], $params_keys); //*判断是否在数组里面
            if (!$Verification_key && $verification_arr[2] == "require") {
                $params[$verification_arr[0]] = "";
            } //*没有的话就自动填充为null(require)
            if (!$Verification_key && $verification_arr[2] == "unrequire") {
                $params[$verification_arr[0]] = "";
            } //*没有的话就自动填充为空字符串(unrequire)
            //!错误类型排除,Verification_key(require->必填,unrequire->不必填)
            if ($verification_arr[2] == "require" || $verification_arr[2] == "unrequire") {
                //?是否设置必填(require)
                //!为空则扔出异常
                if ($verification_arr[2] == "require" && $params[$verification_arr[0]] == "") {
                    throw new HttpException(500, $verification_arr[1] . "不能为空！");
                }
                if ($params[$verification_arr[0]] === null) {
                    throw new HttpException(500, $verification_arr[1] . "不能为空！");
                }
            } else {
                return Tool::msg(500, "参数批量验证器出现未知类型！", [], int: "错误异常！");
            }
        }
        return $params;
    }

    /**
     * 字典读取->读取字典值 //!如果没有字典值，则退回。
     * @author L
     * @access public
     * @param model $model 字典模型层-必填
     * @param array $where 筛选条件-必填
     * @param string $backfont  查询后返回字段-必填
     * @param bool $isfond 是否查询单条数据-选填
     * @return array 放回最终结果
     */
    public static function DictionaryRead(mixed $model /*模型层*/, array $where, string $backfont, bool $isfind = false): array
    {
        if (!$isfind) {
            $value = $model::where($where)->field([$backfont])->select()->toArray();
            if (!$value)
                return Tool::msg(404, "字典查询错误！", [], "请观察字典是否存在对应值！");
            return $value;
        }
        if ($isfind) {
            $value = $model::where($where)->field([$backfont])->find()->toArray();
            if (!$value)
                return Tool::msg(404, "字典查询错误！", [], "请观察字典是否存在对应值！");
            return $value;
        }
    }

    /**
     * 关系无限树->[这里用到的就是关系链，这就像一个家族谱一样不断底接下去] //!请根据数组的索引进行关联 //!请确保每个关联条件都存在相同的值
     * @author L
     * @access public
     * @param array $model [数组]模型层->不限数-必填
     * @param array $relevancetable [数组]关联条件-必填
     * @todo 当一个表与多个表有关联，并且属于是父子关系
     * @return array
     */
    public static function InfiniteRelationTree(array $model, array $relevancetable)
    {
        try {
            $index = implode($model[count($model) - 1])::select()->append(['children'])->toArray(); //*获取最后第一条全部数据
            $i = count($model) - 1; //*获取最后底部索引
            return ServiceTool::getRelationTree($index, $model, $relevancetable, $i); //*调用关系父子递归函数(自封装)
        } catch (\Exception $e) {
            return Tool::msg(404, "树生成错误!", [], "树子结构出问题!");
        } finally {
            if (count($model) != count($relevancetable)) {
                return Tool::msg(404, "无限树关联条件错误!", [], "请观察一下关联条件是否正确!");
            }
        }
    }
    /**
     * 尾随无限树->[这里没有用到关系链,这里用的是数组的push关联尾随追加] //!请根据数组的索引进行关联
     * @author L
     * @access public
     * @param array $model [数组]模型层->不限数-必填
     * @param array $relevancetable [数组]关联条件-必填 
     * @todo 当一个表与多个表有关联，并且属于是阶级关系
     * @return array
     */
    public static function TreeMore(array $model, array $relevancetable)
    {
        //!如果是一阶的话，这个函数是不会提供作用的，反而会给你个报错
        try {
            $index = $model[0][0]::where([$relevancetable[0]])->select(); //*获取首表全部内容
            //*判断模型层是否给值
            $model = count($model) == MODELHAVEVALUE ? 0 : $model;
            $model = match ($model) {
                0 => throw new HttpException(500, "无限树模型层错误!"), //!扔出异常
                $model => $model, //*返回本身
            };
            $number = count($model) - 1; //*最后一条索引
            //*这里利用倒数方法循环
            //todo 利用递归的方法来实现无限树
            for ($i = count($model) - (count($model) - (count($model) - 1)); $i > -1; $i--) {
                //*排除异常
                if (count($model) != count($relevancetable)) {
                    return Tool::msg(404, "无限树关联条件错误!", [], "请观察一下关联条件是否正确!");
                }
                if (count($model) - 1 == 1) //*二阶
                {
                    $value = implode($model[$number])::where([$relevancetable[$number]])->select()->toArray();
                    $index = $index->push($value)->toArray();
                    return $index; //*返回最终结果
                }
                if (count($model) - 1 >= 2) //*多阶或三阶
                {
                    //!$i = 全部model - 头 和 尾
                    if ($i - (count($model) - (count($model) - 1)) == 0) {
                        return;
                    }
                    $value = implode($model[$number])::where([$relevancetable[$number]])->select()->toArray(); //*获取末尾表数据
                    $data = implode($model[$i - 1])::where([$relevancetable[$i - 1]])->select()->push($value); //*将末尾表给倒数第二张表
                    for ($n = 2; $n < count($model) - 1; $n++) {
                        //*排除首表和末尾表和倒数第二张表，这里获取的就是中间部分进行循环push追加
                        $data = implode($model[$i - $n])::where([$relevancetable[$i - $n]])->select()->push($data)->toArray();
                    }
                    $index = $index->push($data)->toArray();
                    return $index; //*返回最终结果
                }
            }
        } catch (\Exception $e) {
            return Tool::msg(404, "树生成错误!", [], "树子结构出问题!");
        }
    }

    /**
     * 一对多关联[多数关联]->(获取无数个表进行追加)  //!请根据数组的索引进行关联
     * @author L
     * @access public
     * @param model $oneselfmodel 自身模型层-必填
     * @param array $oneselfwhere [数组]自身Where筛选-选填
     * @param array $relevancemodel [数组]关联模型层-必填
     * @param array $relevancetable [数组]关联条件-必填
     * @param array $field [数组]筛选查询内容-选填
     * @todo 用于过多关联的信息或者过多关联的承载
     */
    public static function HasMore($oneselfmodel, array $oneselfwhere, array $relevancemodel, array $relevancetable, array $field = [])
    {
        //*异常排除
        if (count($relevancemodel) != count($relevancetable)) {
            throw new HttpException(404, "数组不统一");
        }
        //*获取自身模型层列表
        $index = $oneselfmodel::where($oneselfwhere)
            ->field($field)
            ->select();
        for ($i = 0; $i < count($relevancemodel); $i++) {
            //?判断关联条件是否正确
            if (count($relevancetable[$i]) != 3) {
                return Tool::msg(404, "关联条件错误!");
            }
            $relevancemodel_data = implode($relevancemodel[$i]);
            //*获取关联列表
            $value = $relevancemodel_data::where([$relevancetable[$i]])->select()->toArray();
            $index->push($value)->toArray();
        }
        return $index->toArray(); //*返回全部数据
    }

    /**
     * 一对一关联[连表]->(一个表与另外一个有关联的表拼接) 
     * @author L
     * @access public
     * @param model $oneselfmodel 自身模型层-必填
     * @param string $relevancetablel 关联表名-必填
     * @param array $where 筛选-选填
     * @param array $field 筛选查询内容-选填
     * @param string $Foreign_key 外键-选填-默认dictionary_guid
     * @param string  $Home_key 关联键-选填-默认dictionary_guid
     * @todo 连接与自己有关联的表，并把有关联的条数据追加到一起。
     */
    public static function HasOne($oneselfmodel, string $relevancetablel, array $where = [], array $field = [], string $Foreign_key = 'dictionary_guid', string $Home_key = 'dictionary_guid')
    {
        if ($where != []) {
            $value = $oneselfmodel::where($where)->alias('a')->join("{$relevancetablel} b", "a.{$Foreign_key} = b.{$Home_key}")
                ->field($field)
                ->select();
            return $value;
        } else {
            $value = $oneselfmodel::alias('a')->join("{$relevancetablel} b", "a.{$Foreign_key} = b.{$Home_key}")
                ->field($field)
                ->select();
            return $value;
        }
    }

    /**
     * 树生成[爷->父->子的关系链]
     * @author L
     * @version 0.9
     */
    public static function Tree_Three(
        $model
        /**自身模型层 */
        ,
        array $relevancemodel
        /**关联的模型层 */
        ,
        string $relevancetable,
    ) {
        //?判断关联模型层的数量->因为是爷父子关系，所以长度是2
        if (count($relevancemodel) != 2) {
            throw new HttpException(404, "关联模型层数量错误!");
        }
        $grandfather = $model::field([])
            ->append(['children'])
            ->select()
            ->toArray();
        $father = $relevancemodel[0]::field([])
            ->append(['children'])
            ->select()
            ->toArray();

        for ($i = 0; $i < count($father); $i++) {
            //*获取子内容
            $father_son = $relevancemodel[count($relevancemodel) - 1]::where($relevancetable, $father[$i][$relevancetable])
                ->select()->toArray();
            $father[$i]['children'] = $father_son; //*将子内容给父亲

            for ($a = 0; $a < count($grandfather); $a++) {
                $grandfather[$a]['children'] = $father[$i]; //*将父亲内容给爷爷
            }
        }
        return $grandfather; //*返回全部数据
    }
    /**
     * 生成树->(父与子的关系链)
     * @author L
     * @access public
     * @param model $oneselfmodel 自身模型层-必填
     * @param model $relevancemodel 关联模型层-必填
     * @param string $relevancetable 关联字段-必填
     * @param array|string $oneselfield 父表筛选-选填
     * @param array|string  $relevancefield 子表筛选-选填
     * @return array 父与子的关系树列表数组
     * @todo 用于获取父亲和子表的关系数组。
     * @version 1.0.0
     */
    public static function Tree(
        $oneselfmodel
        /**自身模型层 */
        ,
        $relevancemodel
        /**关联模型层 */
        ,
        string $relevancetable,
        array|string $oneselfield = [],
        array|string $relevancefield = [],
    ): array {
        try {
            //*获取本表的全部数据->提供children字段来储存子表内容
            $data = $oneselfmodel::field($oneselfield)->append(['children'])->select()->toArray();
            for ($i = 0; $i < count($data); $i++) {
                //*获取子内容
                $son = $relevancemodel::where($relevancetable, $data[$i][$relevancetable])->field($relevancefield)
                    ->select()->toArray();
                $data[$i]['children'] = $son;
            }
            return $data; //*返回最后数据
        } catch (\Exception $e) {
            return Tool::msg(404, "树生成错误!", [], "树子结构出问题!");
        }
    }
    /**
     * 获取电脑配置信息
     * @author L
     * @access public
     * @param bool $details 是否详情-选填
     * @return string
     */
    public static function GetComputerInformation(bool $details = false): string
    {
        $Systemtype = php_uname(); //获取电脑操作系统
        $Systemtype = str_replace('Windows NT ALONE 10.0 build 22621 ', '', $Systemtype);
        $Systemtype = str_replace('(', '', $Systemtype);
        $Systemtype = str_replace(')', '', $Systemtype);
        $Systemtype = str_replace('AMD', '系统位数:', $Systemtype);
        $username = get_current_user(); //获取系统用户名
        $ip = Tool::GetIP(); //获取IP地址
        $php = PHP_VERSION; //获取PHP版本

        if ($details) {
            $value = "操作系统:" . $Systemtype . '<br>' . "用户名:" .
                $username . '<br>' . "IP地址:" . $ip . '<br>' . "PHP版本:" . $php;
            return $value;
        } else {
            $value = $Systemtype . '<br>' . $username . '<br>' .
                $ip . '<br>' . $php;
            return $value;
        }
    }
    /**
     * 出栈->(通过递归的使用)[先进后出，后进先出]
     * @author L
     * @access public
     * @param $data 数组(栈)->不管是顺序还是倒序后会进行一次重新倒序
     */
    public static function pop(array $data)
    {
        return array_reverse($data);
    }
    /**
     * 时间线Token验证器->(获取当前日期的Token值)
     * @author L
     * @access public
     * @return string
     */
    public static function Token(): string
    {
        $time = Tool::GetNewDate('Year');
        $value = Service::TokenChange($time);
        return $value;
    }

    /**
     * 中英文字符替换->(中英文字符转换)
     * @author L
     * @access public
     * @param string $character 中文字符character-必填
     */
    public static function ChineseChangeEnglist(string $character)
    {
        $value = Service::ChineseAndEnglish($character);
        return $value;
    }
    /**
     * 下划线转陀峰->(不允许被大写)
     * @author L
     * @access public
     * @param string $data 内容字符串data-必填
     * @param bool $change 是否进行转换change-选填
     */
    public static function ChangeToFeng(string $data, bool $change = true)
    {
        $data = Tool::NotNull($data);
        //判断change是否为true
        if ($change) {
            //去除下划线
            $data = str_replace("_", "", $data);
            // //首字母大写
            $data = ucfirst($data);
            return $data;
        } else {
            $data = str_replace("_", "", $data);
        }
    }
    /**
     * 陀峰转下划线->(不允许被小写)
     * @author L
     * @param string $data 内容字符串data-必填
     * @param string $changedata 改变字段分离的内容changedata-必填
     * @param bool $change 是否进行转换change-选填
     */
    public static function ChangeUnderline(string $data, string $changedata, bool $change = true)
    {
        $data = Tool::NotNull($data);
        //判断change是否为true
        if ($change) {
            $data_len = strlen($data); //get all length
            $changedata_len = strpos($data, $changedata); //获取截取字符首长度get first letter spilt length
            //截加"_"
            $symbol = "_";
            $value = strtolower(substr($data, 0, $changedata_len)) . $symbol . strtolower(substr($data, $changedata_len, $data_len)); //首字母小写lower of first letter
            return $value;
        } else {
            $value = strtolower($data);
            return $value;
        }
    }
    /**
     * 删除目录->(删除指定目录文件夹)
     * @author L
     * @static
     * @date 2023/10/10
     * @param string $url
     * @return mixed
     */
    public static function DeleteDir(string $url)
    {
        $data = rmdir($url);
        return self::msg(200,"删除成功！");
    }
    /**
     * 删除文件->(删除指定文件)
     * @author  L
     * @access public
     * @param string $url 删除文件的路径
     * @todo 主要删除一些不需要的文件或其余内容
     */
    public static function DeleteFile(string $url)
    {
        //?判断文件是否存在
        if (file_exists($url)) {
            $delete = unlink($url); //!删除文件
            if ($delete) {
                return Tool::msg(200, "删除成功!", [], "文件删除成功!");
            } else {
                return Tool::msg(404, "删除成功!", [], "文件删除失败!");
            }
        } else {
            return Tool::msg(404, "文件不存在!");
        }
    }
    /**
     * 自动生成文件夹->(生成指定文件夹)
     * @author L
     * @access public
     * @param string $url 生成路径url-必填
     * @param string $name 生成文件夹名字name-必填
     */
    public static function AutomaticOs(string $url, string $name)
    {
        $url = Tool::NotNull($url);
        $name = Tool::NotNull($name);
        $value = "/" . $name; //二次转换(url)twice change
        $data = $url . $value;
        //判断文件夹是否存在judgment os whether have
        if (!file_exists($data)) {
            mkdir($data); //生成文件夹
            $value = Tool::msg(200, "生成成功!");
            return $value;
        } else {
            $value = Tool::msg(404, "生成失败!");
            return;
        }
    }
    /**
     * 自动生成文件->(指定url路径生成某种文件)
     * @author L
     * @access public
     * @param string $url 路径url-必填
     * @param string $name 文件名字name-必填
     * @param string $suffix 文件后缀suffix-选填
     * @param string $data 写入文件的内容(空或最终结果)data-选填
     */
    public static function AutomaticFile(string $url, string $name, string $suffix = "", string $data = "")
    {
        $url = Tool::NotNull($url);
        $name = Tool::NotNull($name);
        if ($suffix == "") {
            $value = fopen($url . "/" . $name, "w"); //截加
            $value = fwrite($value, $data); //写入
            return $value;
        } else {
            $suffix_value = '.' . $suffix; //二次转换(后缀)twice change
            $name_value = $name . $suffix_value; //拼接
            $value = fopen($url . "/" . $name_value, "a"); //截加
            $value = fwrite($value, $data); //写入
            return $value;
        }
    }
    /**
     * 文件写入->(指定文件写入内容)
     * @author L
     * @access public
     * @param string $url 文件路径url-必填
     * @param string $data 文件写入内容data-选填
     */
    public static function FileWrite(string $url, string $data = "")
    {
        $url = Tool::NotNull($url);

        $file = fopen($url, 'a');
        fwrite($file, $data);
        $value = Tool::msg(200, "写入成功!");
        return $value;
    }
    /**
     * 获取本机IP地址->(获取本机IP地址)
     * @author L 
     * @access public
     */
    public static function GetIP()
    {
        //获取IP地址get IP localhost
        $value = gethostbyname(gethostname());
        return $value;
    }
    /**
     * 获取当前时间->(获取自定类型的时间戳)
     * @author L
     * @access public
     * @param string $type 时间类型(New->获取当前全部时间戳.Year->获取年、月、日的时间戳.Day->获取时、分、秒的时间戳)-选填
     */
    public static function GetNewDate(string $type)
    {
        $type = Tool::NotNull($type);
        switch ($type) {
            case 'New':
                $value = date('y.m.d-H.i:s');
                return $value;
            case 'Year':
                $value = date('y.m.d');
                return $value;
            case 'Day':
                $value = date('H.i:s');
                return $value;
            default:
                return;
        }
    }
    /**
     * 生成唯一id->(guid)
     * @author L
     * @access public
     * @param string $name 传入内容(字段信息)(空或最终结果)name-选填
     * @param bool $isStrong 是否强类型写入(强制写入时间戳Token)isStrong-选填
     */
    public static function AutomaticID(string $name = '',bool $isStrong = false)
    {
        //?获取当前时间戳
        $name = json_encode($name); //解析json
        //避免出现大写(转小写)(数据库对大小写铭感)
        //编译md5散列
        //?是否强类型写入，里面混杂着时间戳
        if($isStrong)
        {
            $newDateTime = self::GetNewDate('New');
            $value = md5($newDateTime.$name);
        }
        else if(!$isStrong)
        {
            $value = md5($name);
        }
        $value = strtolower($value);
        //截加."."
        $symbol = '.';
        $value = substr($value, 0, 5) . $symbol
            . substr($value, 4, 9) . $symbol
            . substr($value, 8, 13) . $symbol
            . substr($value, 12, 12);
        return $value;
    }
    /**
     * Message警示框->(后端给前端和前台接口的返回提示)->多种
     * @author L
     * @access public
     * @param int $code 返回代参code-必填
     * @param string $msg 返回内容msg-必填
     * @param array  $data 成功返回参数(空或最终结果)data-选填
     * @param string $func 返回的方法名称func-选填
     * @return Json 返回JSON数据
     */
    public static function Message(int $code, string $msg, ?array $data = [],?string $func = ""): Json
    {
        return CodeConfig::Code(code: $code, msg: $msg, data: $data,func:$func);
    }
    /**
     * Message警示框->(后端给前端和前台接口的返回提示)->轻便
     * @author L
     * @access public
     * @param int $code 返回代参code-必填
     * @param string $msg 返回内容msg-选填
     * @param array $data 成功返回参数(空或最终结果)data-选填
     * @param string $int 注解int - 选填
     * @return \think\response\Json 返回JSON数据
     */
    public static function msg(int $code, string $msg, ?array $data = [], ?string $int = ""): Json
    {
        $msg = Tool::NotNull($msg);
        $retrurn_data = ['code' => $code];

        switch ($code) {
            case 200:
                /**成功回调 */
                $retrurn_data['msg'] = $msg;
                $retrurn_data['data'] = $data;
                $retrurn_data['int'] = $int;
                break;
            case 404:
                /**失败回调 */
                $retrurn_data['msg'] = $msg;
                $retrurn_data['int'] = $int;
                break;
            case 500:
                /**异常回调 */
                throw new Exception($msg, $code);
            default:
                throw new Exception("code代参出错.", $code);
        }
        return json($retrurn_data);
    }

    /**
     * 批量查询筛选选项器->(可以进行多组搜索功能)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param array $data 批量查询的数组data-必填
     * @param array $order 批量查询的排序order-选填
     */
    public static function GetQueryWhere(
        $model
        /**指定模型曾 */
        ,
        array $data,
        array $order = []
    ) {

        if (!is_array($data))
            throw new Exception("查询错误!");
        if (!is_array($order))
            throw new Exception("查询错误!");
        try {
            $con[] = $data;
            //*默认为空时
            if ($order == []) {
                $value = $model::where($con)
                    ->select()->toArray();
                return $value;
            }
            //!排序类型要求规范，不能超过指定范围(最大长度)
            //*批量排序最大长度:2
            if (count($order) > 2) {
                $value = Tool::msg(404, "排序类型错误!");
                return $value;
            }
            //!如果批量排序长度=1,自动补充->默认值："asc"
            if (count($order) == 1 && $order[0] != "asc" && $order[0] != "desc") {
                $order = [$order[0], "asc"];
                $value = $model::where($con)
                    ->order($order[0], $order[1])->select()->toArray();
                return $value;
            }
            if (count($order) == 2) {
                // $data = Tool::ArrNotNull($data);
                $data = $data == null & $data == "" ? 1 : 2;
                switch ($data) {
                    case 1:
                        $value = Tool::msg(404, "查询失败!");
                        break;
                    case 2:
                        $value = $model::where($con)
                            ->order($order[0], $order[1])->select()->toArray();
                        return $value;
                }
            }
        } catch (\Exception $e) {
            return $model->rollback(); //!失败，回滚！
        }
    }
    /**
     * Get请求url->(请求路径url)
     * @author L
     * @access public
     * @param string $url 请求路径url-必填
     */
    public static function Get(string $url)
    {
        $url = Tool::NotNull($url);
        $value = file_get_contents($url);
        return $value;
    }
    /**
     * 字段数值增值->(只能加一,根据实情增值判断)
     * @author L
     * @access public
     * @param model $model 模型层model-必填
     * @param string $name 字段名字name-必填
     * @param int $number 字段增值的数字number-必填
     * @return Json
     */
    public static function Inc(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number
    ): Json {

        $number = $number < 0 ? 0 : $number;
        $value = $model::where($name, $number)->inc($name)->save();
        $value = Tool::msg(200, '增值成功');
        return $value;
    }
    /**
     * 字段数值减值->(只能减一,根据实情减值判断)
     * @author L
     * @access public
     * @param model $model 模型层model-必填
     * @param string $name 字段名字name-必填
     * @param int $number 字段减值的数字number-必填
     * @return Json
     */
    public static function Dec(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number
    ): Json {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        // if ($numbr < 0) {
        //     $numbr = 0;
        // }
        $number = $number < 0 ? 0 : $number; //不能小于负数
        $value = $model::where($name, $number)->dec($name)->save();
        $value = Tool::msg(200, '减值成功');
        return $value;
    }
    /**
     * 修改指定字段数值->(修改某条字段的数值,指定修改)
     * @author L
     * @access public
     * @param model $model 模型层model-必填
     * @param string $name 修改的字段信息name-必填
     * @param int $number 查询的字段数值number-必填
     * @param int $changeint 修改的字段数值changeint-必填
     * @return Json
     */
    public static function ChangeInt(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number,
        int $changeint
    ): Json {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        $changeint = Tool::NotNull($changeint);
        // //不能超出整数
        // if ($changeint < 0) {
        //     $changeint = 0;
        // }
        $changeint = $changeint < 0 ? 0 : $changeint; //不能小于负数
        $con = [
            $name => $changeint,
        ];
        $value = $model::where($name, $number)->field([
            $name,
        ])->save($con);
        $value = Tool::msg(200, "修改成功");
        return $value;
    }
    /**
     * 修改指定字段字符串->(修改某条字段的字符串,指定修改)
     * @author L
     * @access public
     * @param model $model 模型层model-必填
     * @param string $name 修改的字段信息name-必填
     * @param string $data 查询的字段字符串data-必填
     * @param string $changdata 修改的字段字符串changedata-必填
     * @return Json
     */
    public static function ChangeString(
        $model
        /**指定模型层 */
        ,
        string $name,
        string $data,
        string $changedata
    ): Json {
        $name = Tool::NotNull($name);
        $data = Tool::NotNull($data);
        $changedata = Tool::NotNull($changedata);
        $con = [
            $name => $changedata,
        ];
        $value = $model::where($name, $data)->field([
            $name,
        ])->save($con);
        $value = Tool::msg(200, "修改成功");
        return $value;
    }
    /**
     * 下载文件->(文件下载读取下载内容)
     * @author L
     * @access public
     * @param string $downfile 下载文件downfile-选填
     * @param string $currnotfile 下载命名文件currnotfile-选填
     * @param string $name 命名下载文件name-选填
     * @param int $expirenumber 有效期expirenumber-选填
     * @param bool $force 是否强制force-选填
     * @param bool $getMimeType 获取文件信息getMimeType-选填
     */
    public static function DownFile(
        string $downfile,
        string $currnotfile,
        string $name,
        int $expirenumber = 1000000
        /**默认值 */
        ,
        bool $force = false,
        bool $getMimeType = false
    ) {
        if (!$force && $getMimeType) {
            $value = download($downfile, $currnotfile)->name($name)->expires($expirenumber)->getMimeType($getMimeType);
            return $value;
        }
        if ($force && $getMimeType) {
            $value = download($downfile, $currnotfile)->name($name)->expires($expirenumber)->force($force)->getMimeType($getMimeType);
            return $value;
        }
        if ($force && !$getMimeType) {
            $value = download($downfile, $currnotfile)->name($name)->expires($expirenumber)->force($force);
            return $value;
        }
        $value = download($downfile, $currnotfile)->name($name)->expires($expirenumber);
        return $value;
    }

    /**
     * 自拟报错异常->(自定义报错提示)
     * @author L
     * @access public
     * @param string $message 报错内容Message-选填
     * @param int $code 代参数字code-选填
     * @return void
     */
    public static function WrongPrompt(string $message = '', int $code = 500): void
    {
        $value = abort($code, $message);
    }

    /**
     * 日志->(记录每次执行的方法和时间->在当前时间执行一次?方法)[需要搭配一键生成日志或生成代码执行!]
     * @author L
     * @access public
     * @param string $type 日志类型-必填
     * @param string $Configdata 日志相关配置-选填
     * @test 类型:FUNCTION->__FUNCTION__;CLASS->__CLASS__;INFORMATION->根据自己需求;
     * @todo 根据汇报执行了哪些日志信息，可以是个函数也可以是个类也可以是段信息。
     */
    public static function RunLog(string $type, string $Configdata): string
    {
        $type = Tool::NotNull($type);
        switch ($type) {
            case "FUNCTION":
                $time = Tool::GetNewDate('New');
                $data = "[" . $time . "][FUNCTION]你执行了一次-" . $Configdata . "\n";
                return $data;
            case "CLASS":
                $time = Tool::GetNewDate('New');
                $data = "[" . $time . "][CLASS]调试一次-" . $Configdata . "\n";
                return $data;
            case "INFORMATION":
                $time = Tool::GetNewDate('New');
                $data = "[" . $time . "][INFORMATION]相关信息-" . $Configdata . "\n";
                return $data;
            default:
                $time = Tool::GetNewDate('New');
                $data = "[" . $time . "][NONE]默认-" . $Configdata . "\n";
                return $data;
        }
    }
    /**
     * 一键生成日志->(自动生成日志文件夹(可选)和文件(必选))
     * @author L
     * @access public
     * @param bool $ChoiseOs 选择是否生成日志文件夹ChoiseOs-选填
     * @param string $OsSrc 生成日志文件夹路径OsSrc-选填
     * @param string $FileSrc 生成日志文件路径FileSrc-必填
     * @todo 生成日志的文件夹与文件
     */
    public static function TouchRunLog(bool $ChoiseOs = false, string $OsSrc = "", string $FileSrc = "")
    {
        try {
            // if($FileSrc == "" || $FileSrc == null)
            // {
            //     $value = Tool::WrongP    rompt("生成日志文件路径-必填!");

            // }
            $value = $FileSrc == "" ? 1 : 2;
            switch ($value) {
                case 1:
                    $value = Tool::msg(404, "生成日志路径-必填!");
                    return $value;
                case 2:
                    $OsName = "runlog"; //日志文件夹名字(可以修改)
                    if ($ChoiseOs) {
                        $value = Tool::AutomaticOs($OsSrc, $OsName);
                        $time = Tool::GetNewDate('Year');
                        $value = Tool::AutomaticOs($OsSrc . '/' . $OsName, $time);
                        $newtime = Tool::GetNewDate('Year');
                        $value = Tool::AutomaticFile($OsSrc . '/' . $OsName . '/' . $time, $newtime, 'log');
                        $value = Tool::msg(200, "生成成功!");
                        return $value;
                    }
                    if (!$ChoiseOs) {
                        $OsSrc = null;
                        $value = Tool::AutomaticFile($FileSrc, "RL", 'log', "");
                        $value = Tool::msg(200, "生成成功!");
                        return $value;
                    }
                    break;
            }
        } catch (\Exception $e) {
            return $model->rollback(); //!失败，回滚！
        }
    }
    /**
     * 数据库表生成->(生成原生SQL基本的数据表)
     * @author L
     * @access public
     * @param string $tablename 表名字-必填
     * @return string
     */
    public static function MakeSQLTable(string $tablename): string
    {
        $sql = "CREATE TABLE demo( 
        demo_id  int(0) NOT NULL KEY Auto_Increment,
        demo_guid  varchar(255),
        test 
        demo_create_time datetime ,
        demo_update_time datetime ,
        demo_delete_time datetime  )";
        $sql = str_replace("demo", $tablename, $sql);
        return $sql;
    }


    /**
     * 随机获取全部信息字段内容->(随机获取全部信息中想要的字段信息中的内容)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param string $name 查询字段名字(数字类型)name-必填
     * @param array $field 查询显示的内容field-选填
     * @return Json
     */
    public static function AllRandGetText(
        $model
        /**指定模型层 */
        ,
        string $name,
        array $field
    ): Json {
        $name = Tool::NotNull($name);

        $value = $model::select();
        $value = count($value); //获取总数get all number
        $value = rand(1, $value);
        $value = $model::where($name, $value)->field(
            $field,
        )->select()->toArray();
        $value = Tool::msg(200, '获取成功', $value);
        return $value;
    }
    /**
     * 超出范围自动删除->(限制表字段长度)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param string $name 字段名name-必填
     * @param int $Max 最大限制Max-必填
     */
    public static function OutRandAutoDelete(
        $model
        /**指定模型层 */
        ,
        $name,
        $Max
    ) {
        $name = Tool::NotNull($name);
        $Max = Tool::NotNull($Max);
        $value = $model::where($name, '>', $Max)->select()->delete();
        return $value;
    }
    /**
     * 绑定表字段->(可绑定多种字段)
     * @author L
     * @access public
     * @param string $infomat 前缀字段名字-必填
     * @param string  $behind 后缀字段名字-必填
     */
    public static function Bind(string $infomat, string $behind)
    {
        $infomat = Tool::NotNull($infomat);
        $behind = Tool::NotNull($behind);
        //全部都定义小写
        $infomat = strtolower($infomat);
        $behind = strtolower($behind);
        //截加"-"
        $value = $infomat . "_" . $behind;
        return $value;
    }


    /**
     * 删除腾位->(删除某条字段的时候,其余字段信息自动减去相对数字)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param string $name 腾位字段名字name-必填
     * @param int $number 腾位字段信息的数字number-必填
     */
    public static function DeleteVacate(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        //进行删除
        $value = $model::where($name, $number)->find()->delete();
        //进行腾位
        $data = $model::where($name, ">", $number)
            ->field([
                $name,
            ])
            ->dec($name, 1)->save();
        // if ($value == 1) {
        //     $value = Tool::msg(200, '腾位成功');
        //     return $value;
        // }
        // if ($value == 0) {
        //     $value = Tool::msg(500, '腾位失败');
        //     return $value;
        // }
        $value = $value = 1 ? Tool::msg(200, "腾位成功") : $value = 0 ? Tool::msg(404, "腾位失败") : false;
        if (!$value) {
            $model->rollback(); //!回滚
            return;
        }
        return $value;
    }
    /**
     * 新建腾位->(新建字段信息,后面字段信息腾出一位)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param array $data 创建的数组字段信息data-必填
     * @param string $name 腾位字段名字name-必填
     * @param int $number 腾位字段数字number-必填
     */
    public static function CreateVacate(
        $model
        /**指定模型层 */
        ,
        array $data,
        string $name,
        int $number
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        //新建腾位
        $all = $model::select(); //获取字段总条数
        $count = count($all); //总长度

        $value = $model::where($name, '>=', $number)->inc($name, $count)->save();
        $value = $model::create($data); //创造字段信息
        //腾位不能小于等于0
        if ($number <= 0) {
            $value = $model::where($name, '>', $number)->dec($name, $count - 1)->save();
            $value = $model::where($name, $number)->inc($name, -($number) + 1)->save();
            $value = Tool::msg(200, '腾位成功');
            return $value;
        } else if ($number > $count) //不能超出范围->(自动补充)
        {
            $Endnumber = $number - $count;
            $value = $model::where($name, $number)->dec($name, $Endnumber - 1)->save();
            if ($value == 1) {
                $value = Tool::msg(200, '补充成功');
                return $value;
            }
        } else {
            $value = $model::where($name, '>', $number)->dec($name, $count - 1)->save();
            $value = Tool::msg(200, '腾位成功');
            return $value;
        }
    }
    /**
     * 向前腾位->(与前面的字段信息互换位置)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param string $name 腾位字段名字name-必填
     * @param int $data 互换腾位的字段数字number-必填
     */
    public static function InfontVacate(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        $movenumber = 1000000; //移数
        //防止超出范围
        $value = $model::where($name, $number - 1)->select();
        if ($value == '[]') {
            $value = Tool::msg(404, '腾位失败');
            return $value;
        }
        $value = $model::where($name, $number - 1)->field([
            $name,
        ])->inc($name, $movenumber)->save();
        $data = $model::where($name, $number)->field([
            $name,
        ])->dec($name, 1)->save();
        $value = $model::where($name, $movenumber + $number - 1)->field([
            $name,
        ])->dec($name, $movenumber - 1)->save();
        if ($value == 1) {
            $value = Tool::msg(200, '腾位成功');
            return $value;
        }
        if ($value == 0) {
            $value = Tool::msg(404, '腾位失败');
            return $value;
        }
    }
    /**
     * 向后腾位->(与后面的字段信息互换位置)
     * @author L
     * @access public
     * @param model $model 模型层Model-必填
     * @param string $name 腾位字段名字name-必填
     * @param int $number 腾位字段数字number-必填
     */
    public static function BehindVacate(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number
    ) {

        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        $movenumber = 1000000; //移数

        //防止超出范围
        $value = $model::where($name, $number + 1)->select();
        if ($value == '[]') {
            $data = Tool::msg(404, '腾位失败');
            return $data;
        }
        $value = $model::where($name, $number + 1)->field([
            $name,
        ])->inc($name, $movenumber)->save();

        $data = $model::where($name, $number)->field([
            $name
        ])->inc($name, 1)->save();
        $value = $model::where($name, $movenumber + $number + 1)->field([
            $name
        ])->dec($name, $movenumber + 1)->save();
        //判断
        if ($value == 1) {
            $value = Tool::msg(200, '腾位成功');
            return $value;
        }
        if ($value == 0) {
            $value = Tool::msg(404, '腾位失败');
            return $value;
        }
    }
    /**
     * 编辑腾位->(两处互换位置,修改字段信息,重新腾位)
     * @author L
     *@access public
     *@param model $model 模型层Model-必填
     *@param string $name 腾位字段名字name-必填
     *@param int $number 当前的字段信息的数字number-必填
     *@param int $mainnumber 主要替换的字段信息的数字mainnumber-必填
     *@param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function ExditVacate(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number,
        int $mainnumber,
        array $selectname = []
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);
        $mainnumber = Tool::NotNull($mainnumber);
        $movenumber = 100000; //移数

        if ($number > $mainnumber) {
            $data = $model::where($name, $number)->field($selectname)->select()->toArray();
            $value = $model::where($name, '>=', $number)->inc($name, $movenumber)->save();
            $value = $model::where($name, $mainnumber)->inc($name, $number - $mainnumber)->save();
            $value = $model::where($name, '>', $number)->dec($name, $movenumber - 1)->save();
            $value = $model::where($name, $number + 1)->dec($name, $number)->save();
            $value = $model::where($name, '>', $number)->dec($name, 1)->save();
            $value = Tool::msg(200, "腾位成功", $data);
            return $value;
        }
        if ($number < $mainnumber) {
            $data = $model::where($name, $number)->field($selectname)->select()->toArray();
            $value = $model::where($name, '>=', $mainnumber)->inc($name, $movenumber)->save();
            $value = $model::where($name, $number)->inc($name, $mainnumber - $number)->save();
            $value = $model::where($name, '>', $mainnumber)->dec($name, $movenumber - 1)->save();
            $value = $model::where($name, $mainnumber + 1)->dec($name, $mainnumber)->save();
            $value = $model::where($name, '>', $mainnumber)->dec($name, 1)->save();
            $value = Tool::msg(200, "腾位成功", $data);
            return $value;
        }
        if ($number == $mainnumber) {
            $data = $model::where($name, $number)->field($selectname)->select()->toArray();
            $value = Tool::msg(200, "腾位成功", $data);
            return $value;
        }
    }
    /**
     * 置顶->(将需要的字段设置置顶位置)[注!排序方式](注意排序方式!)
     * !注意排序方式->sort顺序排序
     * @author L
     * @access public
     * @param  $model 模型层Model-必填
     * @param string $name 字段名字name-必填
     * @param int $number 指定数字number-必填
     * @param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function Topping(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number,
        array $selectname = []
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);

        $data = $model::where($name, $number)->field($selectname)->select()->toArray();
        $move = 10000; //移速

        $value = $model::where($name, '>=', 1)->inc($name, $move)->save();
        $value = $model::where($name, $move + $number)->dec($name, $move + $number - 1)->save();
        $value = $model::where($name, '>', 1)->dec($name, $move - 1)->save();
        $value = $model::where($name, '>', $number)->dec($name, 1)->save();

        $value = Tool::msg(200, "置顶成功!", $data);
        return $value;
    }
    /**
     * 置底->(将需要的字段设置置底位置)[注!排序方式](注意排序方式!)
     * !注意排序方式->sort顺序排序
     * @author L
     * @access public
     * @param  $model 模型层Model-必填
     * @param string $name 字段名字name-必填
     * @param int  $number 指定数字number-必填
     * @param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function Bottoming(
        $model
        /**指定模型层 */
        ,
        string $name,
        int $number,
        array $selectname = []
    ) {
        $name = Tool::NotNull($name);
        $number = Tool::NotNull($number);

        $all = $model::select();
        $all = count($all); //获取总数
        $movenumber = 10000; //移数
        $data = $model::where($name, $number)->field($selectname)->select()->toArray();

        $value = $model::where($name, '>', $number)->inc($name, $movenumber)->save();
        $value = $model::where($name, $number)->inc($name, $all - $number)->save();
        $value = $model::where($name, '>', $all)->dec($name, $movenumber + 1)->save();
        $value = Tool::msg(200, "置底成功!", $data);
        return $value;
    }
       /**
     * MySQL(date)类型->(对应封号和解封的Function)[如果需要可以使用]
     * @author L
     * @access public
     * @return string
     */
    public static function MySQLDate(): string
    {
        $timestamp = time(); //获取当前时间戳
        $value = date("y-m-d", $timestamp); //转换成24小时制
        $value = "20" . $value;
        return $value;
    }
    /**
     * MySQL(date)类型->(对应封号和解封的Function)[如果需要可以使用]
     * @author L
     * @access public
     * @return string
     */
    public static function MySQLDataTime(): string
    {
        $timestamp = time(); //获取当前时间戳
        $value = date("Y-m-d H:i:s", $timestamp); //转换成24小时制
        return $value;
    }
    /**
     * MySQL(time)类型->(对应封号和解封的Function)[如果需要可以使用]
     * @author L
     * @access public
     * @return string
     */
    public static function MySQLTime(): string
    {
        $timestamp = time(); //获取当前时间戳
        $value = date("H:i:s", $timestamp); //转换成24小时制
        return $value;
    }
    /**
     * 封号->(对指定某个账号进行封号[软删除])[根据需求进行使用该方法]
     * @author L
     * @access public
     * @param  $model 指定模型层Model-必填
     * @param array $data 查询数组data-必填
     * @param string $name 删除时间的字段名字name-必填
     * @param string $time 封号的时间time-必填
     * @param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function OutDownAdmin(
        $model
        /**指定模型层 */
        ,
        array $data,
        string $name,
        string $time,
        array $selectname = []
    ) {
        $data = Tool::ArrNotNull($data);
        $name = Tool::NotNull($name);
        $time = Tool::NotNull($time);

        $savedata = [
            $name => $time,
        ];
        /**
         * *封号功能排除了监测是否处于封号状态->(本身属于封号功能的效果，所以无论是否已经封号，照样不进行监测!)
         */
        $value = $model::where($data)->save($savedata);
        $data = $model::where($data)->field($selectname)->select()->toArray();
        $value = $value == 0 ? Tool::msg(200, "已封号!", $data) : Tool::msg(200, "封号成功!", $data); //区分判断
        return $value;
    }
    /**
     * 点击事件解封->(事件解封)(对指定某个账号进行解封[解开软删除])[根据需求进行使用该方法](管理员使用)
     * @author L
     * @access public
     * @param  $model 指定模型层Model-必填
     * @param array $data 查询数组data-必填
     * @param string $name 删除时间的字段名字name-必填
     * @param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function ClickUntieAdmin(
        $model
        /**指定模型层 */
        ,
        array $data,
        string $name,
        array $selectname = []
    ) {
        $data = Tool::ArrNotNull($data);
        $name = Tool::NotNull($name);

        $savedata = [
            $name => null,
        ];
        //?监测是否处于封号状态
        $value = $model::where($data)->onlyTrashed()->select();
        if ($value == '[]') {
            $value = Tool::msg(404, "该用户处于非封号状态!");
            return $value;
        }
        $value = $model::where($data)->onlyTrashed()->save($savedata);
        $data = $model::where($data)->field($selectname)->select()->toArray();
        $value = $value == 0 ? Tool::msg(200, "已解封!", $data) : Tool::msg(200, "解封成功!", $data); //区分判断
        return $value;
    }
    /**
     * 按时解封->(时间线解封)(对指定某个账号进行解封[解开软删除])[根据需求进行使用该方法](用户监测使用)
     * @author L
     * @access public
     * @param  $model 指定模型层Model-必填
     * @param array $data 查询数组data-必填
     * @param string $name 删除时间的字段名字name-必填
     * @param string $time 当前时间time(注意:推荐使用MySQLDataTime()封装方法获取当前时间)-必填
     * @param array $selectname 最终获取查询的结果显示数据selectname-选填
     */
    public static function OnTimeUntieAdmin(
        $model
        /**指定模型层 */
        ,
        array $data,
        string $name,
        string $time,
        array $selectname = []
    ) {
        $data = Tool::ArrNotNull($data);
        $name = Tool::NotNull($name);
        $time = Tool::NotNull($time);

        //?监测是否处于封号状态
        $value = $model::where($data)->onlyTrashed()->select();
        if ($value == '[]') {
            $value = Tool::msg(404, "该用户处于非封号状态!");
            return $value;
        }
        $value = $model::where($data)->field($name)->onlyTrashed()->select();
        $content = '[{"' . $name . '":"' . $time . '"}]'; //*搭配字符串
        if ($content >= $value) {
            $savedata = [
                $name => null,
            ];
            $value = $model::where($data)->onlyTrashed()->save($savedata);
            $data = $model::where($data)->field($selectname)->select()->toArray();
            $value = $value == 0 ? Tool::msg(200, "已解封!", $data) : Tool::msg(200, "解封成功!", $data); //区分判断
            return $value;
        } else if ($content < $value) {
            $value = Tool::msg(404, "时间没到达!");
            return $value;
        }
    }
}