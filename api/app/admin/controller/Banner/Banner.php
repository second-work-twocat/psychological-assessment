<?php
/**
 * 轮播图
 */
namespace app\admin\controller\Banner;

use app\BaseController;
use app\model\Banner\Banner as ModelBanner;
use app\admin\controller\Tool;
use app\model\Dictionary\Dictionary as ModelDictionary;
use app\Request;
use config\upload as upload;


class Banner extends BaseController
{
     /**
      * 获取轮播图列表->(后台管理系统)
      */
     public function getBannerList_admin(Request $request)
     {
           (new Tool())->TokenJudgment($request->header("token"));
          $data = [
               'banner_id',
               'banner_src',
               'banner_update_time',
               'dictionary_value'
          ];
          $con[] = ['dictionary_value', '=','后台管理系统'];
          $Banner = Tool::HasOne(ModelBanner::class, 'dictionary',$con , $data)->toArray();
          $rtn = Tool::msg(200, "获取成功!", $Banner);
          return $rtn;
     }
     /**
      * 编辑轮播图
      */
      public function EditBanner(Request $request){
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params,[
               'Id|轮播图id'=>'require',
               'Banner_src|编辑轮播图'=>'require',                    
          ]);
          $change = [
               'banner_src' => $params['Banner_src'],
          ];
          $EditBanner = ModelBanner::where('banner_id',$params['Id'])->save($change);
          return Tool::msg(200,"编辑成功!");
    
      }
      /**
       * 添加轮播图->后台管理系统
       */
      public function AddBanner(Request $request)
      {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params,[
               'Banner_src|编辑轮播图'=>'require',
          ]);
          $dictionary_guid = Tool::AutomaticID('后台管理系统');
          $change = [
                    'banner_src'=>$params['Banner_src'],
                    'banner_position'=>1,
                    'dictionary_guid'=>$dictionary_guid,
          ];
          $AddBanner = ModelBanner::create($change)->select()->toArray();
          return Tool::msg(200,"添加成功!",$AddBanner);
      }
      /**
       * 删除轮播图
       */
      public function DeleteBanner(Request $request){
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params,[
               'Id|轮播图ID'=>'require',
          ]);
          $DeleteBanner = ModelBanner::where('banner_id',$params['Id'])->find()->delete();
          return Tool::msg(200,"删除成功!");
      }
}
