<?php

/**
 * *生成代码
 * *如果错误，请自调
 */

namespace app\admin\controller;

use app\admin\controller\Tool as Tool;
use app\BaseController;
use app\Request;
use app\model\Makecode as ModelMakeCode;

class Root extends BaseController
{
     /**
      * 生成代码
      */
     public function MakeCode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               //*根路径
               'Root|项目根路径' => 'require',
               //*数据库表生成
               'MySQL_table_src|数据表生成路径' => 'require',
               'MySQL_table_name|数据库表名字' => 'require',
               'MySQL_table_name_chinese|数据库表名字(中文)' => 'require',
               'MySQL_font|数据表其余字段' => 'require',
               //*接口类型
               'Interface_type|接口类型' => 'require',
               // *后端生成
               'Back_file_name|后端文件名字' => 'require',
               'Back_control_font|后端控制层需求字段' => 'require',
               'Back_model_font|后端模型层需求字段' => 'require',
               'Back_font|后端部分需求字段' => 'require',
               'Back_normal|后端字段匹配' => 'require',
               'Back_search|后端搜索字段' => 'require',
               //*前端生成
               'Api_content|前端内容' => 'require',
               'Api_List|前端List渲染' => 'require',
               'Api_type|前端类型' => 'require',
               'Api_type_model|前端类型绑定' => 'require',
               'Api_interface|前端接口参数' => 'require',
               'Api_Edit_type|前端编辑类型绑定' => 'require',
               'Api_Edit_interface|前端编辑接口' => 'require',
               'Api_Search|前端搜索' => 'require',
               //*前端文件上传
               // 'Api_Upload_Interface|文件上传接口' => 'require',

               //*写入生成代码信息
               'User|生成代码的人' => 'require',


          ]);
          //?数据库表生成
          //*数据表内容
          $MySQL_Table_Content = Tool::MakeSQLTable($params['MySQL_table_name']);
          //*替换内容
          $MySQL_font = implode($params['MySQL_font']); //转换字符串
          $MySQL_Table_Content = str_replace('test', $MySQL_font, $MySQL_Table_Content);
          // *生成数据表
          $Make_MySQL_table = Tool::AutomaticFile($params['MySQL_table_src'], $params['MySQL_table_name'], 'txt', $MySQL_Table_Content);
          // ?后端生成
          //*获取首字母大写的后端文件名字
          $Back_Name = ucfirst($params['Back_file_name']);
          //*获取内容
          $Back_data = file_get_contents($params['Root'] . "/api/public/Vipe/model.php");
          //*替换内容
          $Back_data = str_replace("Menu", $Back_Name, $Back_data);
          $Back_data = str_replace("menu", $params['MySQL_table_name'], $Back_data);
          $Back_model_font = implode($params['Back_model_font']);
          $Back_data = str_replace("test", $Back_model_font, $Back_data);
          $Back_data = str_replace('title_headers', $params['MySQL_table_name_chinese'], $Back_data);
          //*生成模型层
          $Back_Model = Tool::AutomaticOs($params['Root'] . "/api/app/model", $Back_Name); //生成模型层文件夹
          $Back_Model = Tool::AutomaticFile($params['Root'] . "/api/app/model/" . $Back_Name, $Back_Name, 'php', $Back_data); //生成模型层文件
          //?后端接口转换
          $Interface_api = array(
               "添加" =>
                    '
               /**
                * 添加demo_title
                * @author L
                */
               public function Adddemo_content(Request $request)
               {
                    $params = $request->param();
                    $this->validate($params,[
                         demo_back_control_font
                    ]);
                    //获取guid
                    $guid = Tool::AutomaticID($params[Back_font]);
                    $add = [
                         "demo_test_guid"=>$guid,//填充guid
                         Back_normal
                    ];
                    $Adddemo_content = Modeldemo_content::create($add)->toArray();
                    return Tool::msg(200,"添加成功!",$Adddemo_content);
               }',
               "编辑" =>
                    '
               /**
                * 编辑demo_title
                *@author L
                */
                public function Editdemo_content(Request $request)
                {
                    $params = $request->param();
                    $this->validate($params,[
                         "demo_test_guid|demo_test_guid"=>"require",
                         demo_back_control_font
                    ]);
                    $edit = [
                         Back_normal
                    ];
                    $Editdemo_content = Modeldemo_content::where("demo_test_guid",$params["demo_test_guid"])->find()->save($edit);
                    return Tool::msg(200,"编辑成功!");

                }
                ',
               "删除" =>
                    '
                /**
                 * 删除demo_title
                 * @author L
                 */
                  public function Deletedemo_content(Request $request)
                 {
                    $params = $request->param();
                    $this->validate($params,[
                         "demo_test_guid|demo_test_guid"=>"require",
                    ]);
                    $Deletedemo_content = Modeldemo_content::where("demo_test_guid",$params["demo_test_guid"])->find()->delete();
                    return Tool::msg(200,"删除成功!");
                 }
                ',
          );
          // *解析接口类型
          // $Interface_data = implode($params['Interface_type']);
          $Interface_data = $params['Interface_type'];
          $Interface_data = str_replace("添加", $Interface_api['添加'], $Interface_data);
          $Interface_data = str_replace("编辑", $Interface_api['编辑'], $Interface_data);
          $Interface_data = str_replace("删除", $Interface_api['删除'], $Interface_data);
          //*替换接口类型内容
          $Interface_data = str_replace('demo_title', $params['MySQL_table_name_chinese'], $Interface_data);
          $Back_control_font = implode($params['Back_control_font']);
          $Interface_data = str_replace('demo_back_control_font', $Back_control_font, $Interface_data);
          $Interface_data = str_replace('demo_content', $Back_Name, $Interface_data);
          $Back_narmal = implode($params['Back_normal']);
          $Interface_data = str_replace('Back_normal', $Back_narmal, $Interface_data);
          $back_font_first = $params['Back_font'][0];
          $back_font_first = str_replace(",", "", $back_font_first);
          $Interface_data = str_replace('Back_font', $back_font_first, $Interface_data); //guid的写入
          $Interface_data = str_replace('demo_test', $params['Back_file_name'], $Interface_data);
          //*获取内容
          $Back_controller_data = file_get_contents($params['Root'] . "/api/public/Vipe/controller.php");
          // *替换内容
          $Back_controller_data = str_replace('test', $Interface_data, $Back_controller_data);
          $Back_controller_data = str_replace("title_headers", $params['MySQL_table_name_chinese'], $Back_controller_data);
          $Back_controller_data = str_replace("Menu", $Back_Name, $Back_controller_data);
          // $control_font = implode($params['Back_control_font']);
          // $Back_controller_data = str_replace("Search_font", $control_font, $Back_controller_data);
          $search = implode($params['Back_search']);
          $Back_controller_data = str_replace("Search_con", $search, $Back_controller_data);
          $Back_model_font = implode($params['Back_font']);
          $Back_controller_data = str_replace('back_font', $Back_model_font, $Back_controller_data);
          $Back_controller_data = str_replace('demo_order', $params['Back_file_name'], $Back_controller_data);
          //*生成控制层
          $Back_controller = Tool::AutomaticOs($params['Root'] . "/api/app/admin/controller", $Back_Name); //生成控制层文件夹
          $Back_controller = Tool::AutomaticFile($params['Root'] . "/api/app/admin/controller/" . $Back_Name, $Back_Name, 'php', $Back_controller_data);
          //*生成前端接口
          //?前端接口转换
          $Interface_back = array(
               "添加" =>
                    '
               /**
                * 添加demo_title
                * @author L
                */
                export async function Adddemo_content(data)
                {
                    return await axios.post("/api/demo_content.demo_content/Adddemo_content",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               ',
               "编辑" =>
                    '
               /**
                * 编辑demo_title
                * @author L
                */
                export async function Editdemo_content(data)
                {
                    return await axios.post("/api/demo_content.demo_content/Editdemo_content",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               ',
               "删除" =>
                    '
               /**
                * 删除demo_title
                * @author L
                */
                export async function Deletedemo_content(data)
                {
                    return await axios.post("/api/demo_content.demo_content/Deletedemo_content",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               ',
          );
          $Interface_data = $params['Interface_type'];
          $Interface_data = str_replace("添加", $Interface_back['添加'], $Interface_data);
          $Interface_data = str_replace("编辑", $Interface_back['编辑'], $Interface_data);
          $Interface_data = str_replace("删除", $Interface_back['删除'], $Interface_data);
          //*替换接口类型内容
          $Interface_data = str_replace('demo_title', $params['MySQL_table_name_chinese'], $Interface_data);
          $Interface_data = str_replace('demo_content', $Back_Name, $Interface_data);
          //*获取内容
          $Api_server_data = file_get_contents($params['Root'] . "/api/public/Vipe/api_interface.js");
          //*替换内容
          $Api_server_data = str_replace("test", $Interface_data, $Api_server_data);
          $Api_server_data = str_replace("title_headers", $params['MySQL_table_name_chinese'], $Api_server_data);
          $Api_server_data = str_replace("Menu", $Back_Name, $Api_server_data);
          //*生成前端接口
          $Api_server = Tool::AutomaticFile($params['Root'] . "/back/src/service/", $Back_Name, 'js', $Api_server_data);

          //*前端页面生成
          //?前端选择项页面转换
          $Api_Select_type = array(
               "添加" => ' <el-button type="success" @click="AddBannerView=true">添加</el-button>',
               "编辑" => '',
               "删除" => '',
          );
          $Api_select = $params['Interface_type'];
          //*转换选择项
          $Api_select = str_replace("添加", $Api_Select_type['添加'], $Api_select);
          $Api_select = str_replace("编辑", $Api_Select_type['编辑'], $Api_select);
          $Api_select = str_replace("删除", $Api_Select_type['删除'], $Api_select);
          //?前端操作项页面转换
          $Api_caozuo_type = array(
               "添加" => '',
               "编辑" => "<el-button type='success' @click='handleUpdate(scope.row); EditBannerView = true'>编辑</el-button>\n",
               "删除" => "<el-button type='success' @click='handleUpdate(scope.row); DeleteBannerView = true'>删除</el-button>\n",
          );
          $Api_caozuo = $params['Interface_type'];
          //*转换操作项
          $Api_caozuo = str_replace("添加", $Api_caozuo_type['添加'], $Api_caozuo);
          $Api_caozuo = str_replace("编辑", $Api_caozuo_type['编辑'], $Api_caozuo);
          $Api_caozuo = str_replace("删除", $Api_caozuo_type['删除'], $Api_caozuo);
          //?前端引入项页面转换
          $Api_import_type = array(
               "添加" => "import AddBanner from './component/AddBanner.vue'\n",
               "编辑" => "import EditBanner from './component/EditBanner.vue'\n",
               "删除" => "import DeleteBanner from './component/DeleteBanner.vue'\n",
          );
          $Api_import = $params['Interface_type'];
          //*转换引入项
          $Api_import = str_replace("添加", $Api_import_type['添加'], $Api_import);
          $Api_import = str_replace("编辑", $Api_import_type['编辑'], $Api_import);
          $Api_import = str_replace("删除", $Api_import_type['删除'], $Api_import);
          //?前端视图层页面转换
          $Api_views_type = array(
               "添加" => "<AddBanner v-model='AddBannerView'/>\n",
               "编辑" => "<EditBanner v-model='EditBannerView' :data='Row'/>\n",
               "删除" => "<DeleteBanner v-model='DeleteBannerView' :data='Row' />\n",
          );
          $Api_views = $params['Interface_type'];
          //*转换视图层项
          $Api_views = str_replace("添加", $Api_views_type['添加'], $Api_views);
          $Api_views = str_replace("编辑", $Api_views_type['编辑'], $Api_views);
          $Api_views = str_replace("删除", $Api_views_type['删除'], $Api_views);
          //?前端引入状态页面转换
          $Api_import_status = array(
               "添加" => "const AddBannerView = ref(false)\n",
               "编辑" => "const EditBannerView = ref(false)\n",
               "删除" => "const DeleteBannerView = ref(false)\n",
          );
          $Api_status = $params['Interface_type'];
          //*转换状态项
          $Api_status = str_replace("添加", $Api_import_status['添加'], $Api_status);
          $Api_status = str_replace("编辑", $Api_import_status['编辑'], $Api_status);
          $Api_status = str_replace("删除", $Api_import_status['删除'], $Api_status);
          //*获取内容
          $Api = file_get_contents($params['Root'] . "/api/public/Vipe/api.vue");
          //*替换选择项
          $Api = str_replace("add_view", $Api_select, $Api);
          //*替换搜索项
          $Search = implode($params['Api_Search']);
          $Api = str_replace('add_search', $Search, $Api);
          //*替换字段绑定
          $Bind = implode($params['Api_type_model']);
          $Api = str_replace('demo_font_bind', $Bind, $Api);
          //*替换搜索接口
          $Search_interface = implode($params['Api_interface']);
          $Api = str_replace("Search_interface", $Search_interface, $Api);
          //*替换操作项目
          $Api = str_replace("demo_view", $Api_caozuo, $Api);
          //*替换引入项目
          $Api = str_replace("demo_import", $Api_import, $Api);
          //*替换视图层项目
          $Api = str_replace("demos_views", $Api_views, $Api);
          //*替换引入状态
          $Api = str_replace('demo_status', $Api_status, $Api);
          //*替换内容
          $Api = str_replace("Banner", $Back_Name, $Api);
          //*面包屑
          $Api = str_replace("title_headers", $params['MySQL_table_name_chinese'], $Api);
          //*替换字段
          $Api = str_replace("demo_order", $params['Back_file_name'], $Api);
          //*List表渲染
          $Api_List = implode($params['Api_List']);
          $Api = str_replace("demo_List", $Api_List, $Api);

          //*生成前端页面
          $Api_page = Tool::AutomaticOs($params['Root'] . "/back/src/views/Index", $Back_Name); //生成文件夹
          $Api_page = Tool::AutomaticFile($params['Root'] . "/back/src/views/Index/" . $Back_Name, 'index', 'vue', $Api); //生成页面
          //?生成子组件
          $Api_page = Tool::AutomaticOs($params['Root'] . "/back/src/views/Index/" . $Back_Name, "component"); //生成子组件文件夹
          //?生成添加功能
          //*获取内容
          $Api_add_data = file_get_contents($params['Root'] . "/api/public/Vipe/Addapi.vue");
          //*替换内容
          $Api_type = implode($params['Api_type']);
          $Api_type_model = implode($params['Api_type_model']);
          $Api_add_data = str_replace('demo_form', $Api_type, $Api_add_data);
          $Api_add_data = str_replace('demo_bind', $Api_type_model, $Api_add_data);
          $Api_add_data = str_replace('轮播图', $params['MySQL_table_name_chinese'], $Api_add_data);
          //*替换文件上传
          $Api_add_data = str_replace('UploadNamePictureCardPreview', implode($params['Api_Upload_Interface']), $Api_add_data);
          $Api_add_data = str_replace('Banner', $Back_Name, $Api_add_data);
          $Api_add_data = str_replace(':file-list="[{ name: "", url: demo_img }]"', "", $Api_add_data);
          $Api_interface = implode($params['Api_interface']);
          $Api_add_data = str_replace('interface_data', $Api_interface, $Api_add_data);
          //*生成添加功能
          $Api_add_page = Tool::AutomaticFile($params['Root'] . "/back/src/views/Index/" . $Back_Name . "/component", "Add" . $Back_Name, 'vue', $Api_add_data);
          //?生成编辑功能
          //*获取内容
          $Api_edit_data = file_get_contents($params['Root'] . "/api/public/Vipe/Editapi.vue");
          //*替换内容
          $Api_type = implode($params['Api_Edit_type']);

          $Api_edit_data = str_replace('Eidt_demo_form', $Api_type, $Api_edit_data);
          $Api_edit_data = str_replace('轮播图', $params['MySQL_table_name_chinese'], $Api_edit_data);
          $Api_edit_data = str_replace('demo_test', $params['Back_file_name'], $Api_edit_data);
          $Api_edit_data = str_replace('Banner', $Back_Name, $Api_edit_data);
          $Api_interface = implode($params['Api_Edit_interface']);
          $Api_edit_data = str_replace('interface_data', $Api_interface, $Api_edit_data);
          //*生成编辑功能
          $Api_edit_page = Tool::AutomaticFile($params['Root'] . "/back/src/views/Index/" . $Back_Name . "/component", "Edit" . $Back_Name, 'vue', $Api_edit_data);
          //?生成删除功能
          //*获取内容
          $Api_delete_data = file_get_contents($params['Root'] . "/api/public/Vipe/Deleteapi.vue");
          //*替换内容
          $Api_delete_data = str_replace('菜单', $params['MySQL_table_name_chinese'], $Api_delete_data);
          $Api_delete_data = str_replace('Menu', $Back_Name, $Api_delete_data);
          $Api_delete_data = str_replace('Banner', $Back_Name, $Api_delete_data);
          $Api_delete_data = str_replace('demo_test', $params['Back_file_name'], $Api_delete_data);
          //*生成删除功能
          $Api_delete_page = Tool::AutomaticFile($params['Root'] . "/back/src/views/Index/" . $Back_Name . "/component", "Delete" . $Back_Name, 'vue', $Api_delete_data);
          //*获取内容
          $Api_History_data = file_get_contents($params['Root'] . "/api/public/Vipe/Historyapi.vue");
          //*替换内容
          $Api_History_data = str_replace('demo_List', $Api_List, $Api_History_data);
          $Api_History_data = str_replace('Banner', $Back_Name, $Api_History_data);
          $Api_History_data = str_replace('demo_order', $params['Back_file_name'], $Api_History_data);
          //*生成历史记录功能
          $Api_History_page = Tool::AutomaticFile($params['Root'] . "/back/src/views/Index/" . $Back_Name . "/component", "History" . $Back_Name, 'vue', $Api_History_data);






          /**
           * *写入生成代码表信息->必须要！
           */
          $data = [
               'makecode_name' => $Back_Name,
               'makecode_user' => $params["User"],
               'makecode_url' => $params["Root"],
               'makecode_type' => 1,
          ];
          $data = ModelMakeCode::create($data);
          return;
     }
     /**
      * 删除区块
      * @author L
      */
     public function DeleteBlock(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'type|生成代码类型'=>'require',
               "name|区块名字" => 'require',
               "url|代码路径" => 'require',
          ]);
          /**
           * *删除生成代码表信息->必须要！
           */
          $data = ModelMakeCode::where('makecode_name', $params["name"])->find()->delete();
          /**
           * 删除前台区块
           */
          if($params["type"] == 1)
          {
          //*删除后端控制层
          $delete_controller_php = Tool::DeleteFile($params["url"] . "/api/app/admin/controller/" . $params["name"] . "/" . $params["name"] . ".php");
          $delete_controller = Tool::DeleteDir($params["url"] . "/api/app/admin/controller/" . $params["name"]);
          //*删除后端模型层
          $delete_model_php = Tool::DeleteFile($params["url"] . "/api/app/model/" . $params["name"] . "/" . $params["name"] . ".php");
          $delete_model = Tool::DeleteDir($params["url"] . "/api/app/model/" . $params["name"]);
          //*删除前台接口
          $delete_back_interface = Tool::DeleteFile($params["url"] . "/back/src/service/" . $params["name"] . ".js");
          //*删除前台页面组件
          $delete_back_page_composen = Tool::DeleteFile($params["url"] . "/back/src/views/Index/" . $params["name"] . "/component/Add" . $params["name"] . ".vue");
          $delete_back_page_composen = Tool::DeleteFile($params["url"] . "/back/src/views/Index/" . $params["name"] . "/component/Delete" . $params["name"] . ".vue");
          $delete_back_page_composen = Tool::DeleteFile($params["url"] . "/back/src/views/Index/" . $params["name"] . "/component/Edit" . $params["name"] . ".vue");
          $delete_back_page_composen = Tool::DeleteFile($params["url"] . "/back/src/views/Index/" . $params["name"] . "/component/History" . $params["name"] . ".vue");
          $delete_back_page_composen = Tool::DeleteDir($params["url"] . "/back/src/views/Index/" . $params["name"] . "/component");
          //*删除主页面
          $delete_back_page_index = Tool::DeleteFile($params["url"] . "/back/src/views/Index/" . $params["name"] . "/index.vue");
          //*删除文件夹
          $delete_back_dir = Tool::DeleteDir($params["url"] . "/back/src/views/Index/" . $params["name"]);
          return;
     }
     /**
      * 删除前端Api区块
      */
     else if($params["type"] == 2)
     {
          //*删除前端接口
          $delete_back_interface = Tool::DeleteFile($params["url"]."/api/app/api/controller/".$params["name"]."/".$params["name"].".php");
          $delete_back_interface = Tool::DeleteDir($params["url"]."/api/app/api/controller/".$params["name"]);
          return;

     }

        
     }

     /**
      * 生成前端Api接口
      * @author L
      */
     public function MakeApi(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'Root|项目根路径' => 'require',
               'MySQL_table_name_chinese|数据库表名字(中文)' => 'require',
               'Back_file_name|后端文件名字' => 'require',
               'Back_font|后端部分需求字段' => 'require',
               'User|生成代码的人' => 'require',
          ]);


          //*获取首字母大写的后端文件名字
          $Back_Name = ucfirst($params['Back_file_name']);


          //*获取ApiController内容
          $Back_controller_data = file_get_contents($params['Root'] . "/api/public/Vipe/apicontroller.php");
          // *替换内容
          $Back_controller_data = str_replace("title_headers", $params['MySQL_table_name_chinese'], $Back_controller_data);
          $Back_controller_data = str_replace("Menu", $Back_Name, $Back_controller_data);
          $Back_model_font = implode($params['Back_font']);
          $Back_controller_data = str_replace('back_font', $Back_model_font, $Back_controller_data);
          $Back_controller_data = str_replace('demo_order', $params['Back_file_name'], $Back_controller_data);
          //*生成Api接口
          $Back_controller = Tool::AutomaticOs($params['Root'] . "/api/app/api/controller", $Back_Name); //生成控制层文件夹
          $Back_controller = Tool::AutomaticFile($params['Root'] . "/api/app/api/controller/" . $Back_Name, $Back_Name, 'php', $Back_controller_data);


          /**
           * *写入生成代码表信息->必须要！
           */
          $data = [
               'makecode_name' => $Back_Name,
               'makecode_user' => $params["User"],
               'makecode_url' => $params["Root"],
               'makecode_type' => 2,
          ];
          $data = ModelMakeCode::create($data);

          return;
     }

     /**
      * 生成前端登录页面接口
      */
      public function MakeBeforePage_Login(Request $request)
      {
          $params = $request->param();
          $this->validate($params,[
               "type|类型"=>'require',
               "Name|文件名字"=>'require',
               'Src|生成路径'=>'require',
               "Root|根"=>'require',
          ]);
          //*区分类型
          //生成HTML
          if($params["type"] == 1)
          {
               //读取内容
               $content = file_get_contents($params["Root"]."/api/public/Vipe/Before/Login/HTML/Login.html");
               $content = str_replace("Login_title",$params["Name"],$content);
               $data = Tool::AutomaticFile($params["Src"],$params["Name"],"html",$content);
          }
          //生成vue
          else if($params["type"] ==2)
          {
                 //读取内容
                 $content = file_get_contents($params["Root"]."/api/public/Vipe/Before/Login/Vue3Vite/Login.vue");
                 $content = str_replace("LOGIN_TITLE",$params["Name"],$content);
                 $data = Tool::AutomaticFile($params["Src"],$params["Name"],"vue",$content);
          }
          return Tool::Message(200,"生成成功！");
      }


      /**
       * 生成前端其他页面
       */
      public function MakeBeforePage_OverPage(Request $request)
      {
          $params = $request->param();
          $this->validate($params,[
               "Name|文件名字"=>'require',
               'Src|生成路径'=>'require',
               "Root|根"=>'require',
          ]);
          // *读取内容
          $content = file_get_contents($params["Root"]."/api/public/Vipe/Public/".$params["Name"]);
          // *生成页面
          $data = Tool::AutomaticFile($params["Src"],$params["Name"],data:$content);
          return Tool::Message(200,"生成成功！");
      }
}