<?php

/**
 * 折叠面板
 */

namespace app\admin\controller\Collapse;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use app\model\Collapse\Collapse as ModelCollapse;

class Collapse extends BaseController
{
     /**
      * 获取折叠面板
      * @author L
      */
     public function getCollapse(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $Collapse = ModelCollapse::field([
               'collapse_id',
               'collapse_title',
               'collapse_content',
               'collapse_create_time',
          ])
          ->limit(0,4)//!限制4条数据
          ->select()->toArray();
          return Tool::msg(200, "获取成功!", $Collapse);
     }
     /**
      * 编辑折叠面板
      * @author L
      */
      public function EditCollapse(Request $request)
      {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params,[
               'Id|折叠面板id' =>'require',
               'title|折叠面板标题'=>'require',
               'content|折叠面板内容'=>'require',
          ]);
          $change =[
               'collapse_title'=>$params['title'],
               'collapse_content'=>$params['content'],
          ];
          $EditCollapse = ModelCollapse::where('collapse_id',$params['Id'])->find()->save($change);
          return Tool::msg(200,"编辑成功!");
      }

}
