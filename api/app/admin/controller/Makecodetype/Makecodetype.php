<?php
/**
 * 生成代码类型
 * 生成代码-L
 */
namespace app\admin\controller\Makecodetype;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\model\Makecodetype\Makecodetype as ModelMakecodetype;
use app\Request;
use think\console\command\make\Model;
use config\upload as upload;



class Makecodetype extends BaseController
{
     /**
      * 获取生成代码类型列表
      * @author L
      */
     public function getMakecodetypeList(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $MakecodetypeList = ModelMakecodetype::
               field([
                    'makecodetype_guid',
                    'makecodetype_create_time',

                    'makecodetype_name',
                    'makecodetype_suffix',

               ])
               ->order('makecodetype_create_time', 'desc')
               ->select()
               ->toArray();
          return Tool::msg(200, "获取成功!", $MakecodetypeList);
     }
     /**
      * 搜索生成代码类型
      * @author L
      */
     public function SearchMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [

          ]);
          $model = ModelMakecodetype::class;
          $data = [
               ["makecodetype_name", "LIKE", "%" . $params["makecodetype_name"] . "%"],
               ["makecodetype_suffix", "LIKE", "%" . $params["makecodetype_suffix"] . "%"],

          ];

          $Search = Tool::GetQueryWhere($model, $data, ['makecodetype_create_time', 'desc']);
          return Tool::msg(200, "获取成功!", $Search);
     }
     /**
      * 批量删除生成代码类型
      * @author L
      */
     public function BatchDeleteMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               "select_Makecodetype|select_Makecodetype" => "require",
          ]);
          foreach ($params['select_Makecodetype'] as $value) {
               $end = ModelMakecodetype::where("makecodetype_guid", $value['makecodetype_guid'])->select()->delete();
          }
          return Tool::msg(200, "删除成功!", [], "批量删除想要删除的对象!");
     }
     /**
      * 获取生成代码类型历史记录->(软删除)
      * @author  L
      */
     public function getTrasheMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $getTrashe = ModelMakecodetype::
               field([
                    'makecodetype_guid',
                    'makecodetype_delete_time',

                    'makecodetype_name',
                    'makecodetype_suffix',

               ])->onlyTrashed()
               ->order('makecodetype_create_time', 'desc')
               ->select()->toArray();
          return Tool::msg(200, "获取成功!", $getTrashe);


     }
     /**
      * 恢复生成代码类型历史记录
      * @author L
      */
     public function RestoreTrasheMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               "makecodetype_guid|makecodetype_guid" => "require",
          ]);
          $Restore = ModelMakecodetype::where('makecodetype_guid', $params['makecodetype_guid'])->onlyTrashed()->find()->restore();
          return Tool::msg(200, "恢复成功!");

     }
     /**
      * 彻底删除生成代码类型历史记录->(彻底删除)
      * @author  L
      */
     public function ForceTrasheMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               "makecodetype_guid|makecodetype_guid" => "require",
          ]);
          $Restore = ModelMakecodetype::where('makecodetype_guid', $params['makecodetype_guid'])->onlyTrashed()->find()->force()->delete();
          return Tool::msg(200, "删除成功!");
     }


     /**
      * 添加生成代码类型
      * @author L
      */
     public function AddMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               'makecodetype_name' => 'require',
               'makecodetype_suffix' => 'require',

          ]);
          //获取guid
          $guid = Tool::AutomaticID($params['makecodetype_name'
          ]);
          $add = [
               "makecodetype_guid" => $guid,
               //填充guid
               'makecodetype_name' => $params['makecodetype_name'],
               'makecodetype_suffix' => $params['makecodetype_suffix'],

          ];
          $AddMakecodetype = ModelMakecodetype::create($add)->toArray();
          return Tool::msg(200, "添加成功!", $AddMakecodetype);
     }
     /**
      * 编辑生成代码类型
      *@author L
      */
     public function EditMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               "makecodetype_guid|makecodetype_guid" => "require",
               'makecodetype_name' => 'require',
               'makecodetype_suffix' => 'require',

          ]);
          $edit = [
               'makecodetype_name' => $params['makecodetype_name'],
               'makecodetype_suffix' => $params['makecodetype_suffix'],

          ];
          $EditMakecodetype = ModelMakecodetype::where("makecodetype_guid", $params["makecodetype_guid"])->find()->save($edit);
          return Tool::msg(200, "编辑成功!");

     }

     /**
      * 删除生成代码类型
      * @author L
      */
     public function DeleteMakecodetype(Request $request)
     {
          (new Tool())->TokenJudgment($request->header("token"));
          $params = $request->param();
          $this->validate($params, [
               "makecodetype_guid|makecodetype_guid" => "require",
          ]);
          $DeleteMakecodetype = ModelMakecodetype::where("makecodetype_guid", $params["makecodetype_guid"])->find()->delete();
          return Tool::msg(200, "删除成功!");
     }





}