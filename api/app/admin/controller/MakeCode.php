<?php

namespace app\admin\controller;
use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\model\Makecode as ModelMakeCode;



class MakeCode extends BaseController
{

        /**
         * 获取生成代码列表
         * @author L
         */
        public function getMakeCodeList()
        {
                $get = ModelMakeCode::field([
                        'makecode_guid',
                        'makecode_name',
                        'makecode_url',
                        'makecode_user',
                        'makecode_type',
                ])
                ->order("makecode_create_time","asc")
                ->select()->toArray();
                return Tool::msg(200,"获取成功!",$get);
        }


}