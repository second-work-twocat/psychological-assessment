<?php
/**
 * 上传生成代码
 * 生成代码-L
 */
namespace app\admin\controller\Uploadmakecode;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\model\Uploadmakecode\Uploadmakecode as ModelUploadmakecode;
use app\Request;
use think\console\command\make\Model;
use config\upload as upload;
use app\model\Makecodetype\Makecodetype as ModelMakecodetype;

class Uploadmakecode extends BaseController
{
     /**
      * 获取单条上传生成代码
      */
     public function getItemUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'type|类型' => 'require',
               'name|名字' => 'require',
          ]);
          $con[] = ['uploadmakecode_type', '=', $params["type"]];
          $con[] = ['uploadmakecode_name', '=', $params["name"]];
          $item = ModelUploadmakecode::where($con)->find()->toArray();
          return Tool::Message(200, "获取成功！", $item);
     }
     /**
      * 获取上传生成代码列表
      * @author L
      */
     public function getUploadmakecodeList()
     {
          $UploadmakecodeList = ModelUploadmakecode::
               field([
                    'uploadmakecode_guid',
                    'uploadmakecode_create_time',

                    'uploadmakecode_name',
                    'uploadmakecode_img',
                    'uploadmakecode_type',

               ])
               ->order('uploadmakecode_create_time', 'desc')
               ->select()
               ->toArray();
          // 关联
          for ($i = 0; $i < count($UploadmakecodeList); $i++) {
               $UploadmakecodeList[$i]["makecodetype_name"] = ModelMakecodetype::where("makecodetype_guid", $UploadmakecodeList[$i]["uploadmakecode_type"])->find()->toArray()["makecodetype_name"];
          }
          return Tool::msg(200, "获取成功!", $UploadmakecodeList);
     }

     /**
      * 搜索上传生成代码
      * @author L
      */
     public function SearchUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [

          ]);
          $model = ModelUploadmakecode::class;
          $data = [
               // ["uploadmakecode_name", "LIKE", "%" . $params["uploadmakecode_name"] . "%"],
               // ["uploadmakecode_img", "LIKE", "%" . $params["uploadmakecode_img"] . "%"],
               ["uploadmakecode_type", "=", $params["uploadmakecode_type"]],
          ];
          $Search = Tool::GetQueryWhere($model, $data, ['uploadmakecode_create_time', 'desc']);
          return Tool::msg(200, "获取成功!", $Search);
     }
     /**
      * 批量删除上传生成代码
      * @author L
      */
     public function BatchDeleteUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               "select_Uploadmakecode|select_Uploadmakecode" => "require",
          ]);
          foreach ($params['select_Uploadmakecode'] as $value) {
               $end = ModelUploadmakecode::where("uploadmakecode_guid", $value['uploadmakecode_guid'])->select()->delete();
          }
          return Tool::msg(200, "删除成功!", [], "批量删除想要删除的对象!");
     }
     /**
      * 获取上传生成代码历史记录->(软删除)
      * @author  L
      */
     public function getTrasheUploadmakecode()
     {
          $getTrashe = ModelUploadmakecode::
               field([
                    'uploadmakecode_guid',
                    'uploadmakecode_delete_time',
                    'uploadmakecode_name',
                    'uploadmakecode_img',
                    'uploadmakecode_type',

               ])->onlyTrashed()
               ->order('uploadmakecode_create_time', 'desc')
               ->select()->toArray();
          return Tool::msg(200, "获取成功!", $getTrashe);


     }
     /**
      * 恢复上传生成代码历史记录
      * @author L
      */
     public function RestoreTrasheUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               "uploadmakecode_guid|uploadmakecode_guid" => "require",
          ]);
          $Restore = ModelUploadmakecode::where('uploadmakecode_guid', $params['uploadmakecode_guid'])->onlyTrashed()->find()->restore();
          return Tool::msg(200, "恢复成功!");

     }
     /**
      * 彻底删除上传生成代码历史记录->(彻底删除)
      * @author  L
      */
     public function ForceTrasheUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               "uploadmakecode_guid|uploadmakecode_guid" => "require",
          ]);
          $Restore = ModelUploadmakecode::where('uploadmakecode_guid', $params['uploadmakecode_guid'])->onlyTrashed()->find()->force()->delete();
          return Tool::msg(200, "删除成功!");
     }


     /**
      * 添加上传生成代码
      * @author L
      */
     public function AddUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'uploadmakecode_name' => 'require',
               'uploadmakecode_img' => 'require',
               'uploadmakecode_type' => 'require',

          ]);
          //获取guid
          $guid = Tool::AutomaticID($params['uploadmakecode_name'
          ]);
          $add = [
               "uploadmakecode_guid" => $guid,
               //填充guid
               'uploadmakecode_name' => $params['uploadmakecode_name'],
               'uploadmakecode_img' => $params['uploadmakecode_img'],
               'uploadmakecode_type' => $params['uploadmakecode_type'],

          ];
          $AddUploadmakecode = ModelUploadmakecode::create($add)->toArray();
          return Tool::msg(200, "添加成功!", $AddUploadmakecode);
     }
     /**
      * 编辑上传生成代码
      *@author L
      */
     public function EditUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               "uploadmakecode_guid|uploadmakecode_guid" => "require",
               'uploadmakecode_name' => 'require',
               'uploadmakecode_img' => 'require',
               'uploadmakecode_type' => 'require',

          ]);
          $edit = [
               'uploadmakecode_name' => $params['uploadmakecode_name'],
               'uploadmakecode_img' => $params['uploadmakecode_img'],
               'uploadmakecode_type' => $params['uploadmakecode_type'],

          ];
          $EditUploadmakecode = ModelUploadmakecode::where("uploadmakecode_guid", $params["uploadmakecode_guid"])->find()->save($edit);
          return Tool::msg(200, "编辑成功!");

     }

     /**
      * 删除上传生成代码
      * @author L
      */
     public function DeleteUploadmakecode(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               "uploadmakecode_guid|uploadmakecode_guid" => "require",
          ]);
          $DeleteUploadmakecode = ModelUploadmakecode::where("uploadmakecode_guid", $params["uploadmakecode_guid"])->find()->delete();
          return Tool::msg(200, "删除成功!");
     }





}