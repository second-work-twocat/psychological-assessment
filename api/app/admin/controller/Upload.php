<?php
/**
 * 文件上传
 * @author L
 */

namespace app\admin\controller;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use config\upload as Configupload;

class Upload extends BaseController
{
    //*上传文件
    public function Upload(Request $request)
    {
        /**
         * 接受需求参数
         * @author L
         */
        $params = $request->param();
        $this->validate($params, [
            'name|名字' => 'require',
            // 'type|类型'=>'require',
            'size|大小' => 'require',
            'file|文件' => 'require',
        ]);

        $file_data = $params['file'];
        //图片上传
        if ($params['type'] == "image/jpeg" || $params['type'] == "image/png") {
            //*如果传入的file是图片，那么会经过base64编码
            $file = base64_decode($file_data); //todo 解析
            //*上传图片
            $Upload = Tool::AutomaticFile((new Configupload)->uploadImageSrc, $params['name'], "", $file);
        }
        //文件上传
        else if ($params["type"] == "text/html") {
            $file = base64_decode($file_data); //todo 解析
            // $file = file_get_contents($file_data);
            //*上传文件
            $Upload = Tool::AutomaticFile((new Configupload)->uploadMakeCodeSrc, $params['name'], "", $file);
        } else {
            $file = base64_decode($file_data); //todo 解析
            // $file = file_get_contents($file_data);
            //*上传文件
            $Upload = Tool::AutomaticFile((new Configupload)->uploadMakeCodeSrc, $params['name'], "", $file);
        }
        return Tool::msg(200, "上传成功!");
    }
    //*取消上传文件
    public function CloseUpload(Request $request)
    {
        /**
         * 接受需求参数
         * @author L
         */
        $params = $request->param();
        $this->validate($params, [
            'name|名字' => 'require',
            // 'type|类型'=>'require',
        ]);
        if ($params['type'] == "image/jpeg" || $params['type'] == "image/png") {
            $close = Tool::DeleteFile((new Configupload)->uploadImageSrc . "/" . $params['name']);

        } else if ($params["type"] == "text/html") {
            $close = Tool::DeleteFile((new Configupload)->uploadMakeCodeSrc . "/" . $params['name']);
        } else {
            $close = Tool::DeleteFile((new Configupload)->uploadMakeCodeSrc . "/" . $params['name']);
        }
        return Tool::msg(200, "取消上传!");
    }

}