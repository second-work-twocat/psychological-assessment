<?php
/**
 * 当事人
 */
namespace app\admin\controller\Party;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\model\Party\Party as ModelParty;
use app\model\Login as ModelLogin;
use app\Request;

class Party extends BaseController
{
  /**
   * 申请当事人
   * @author  L
   * todo 申请当事人，可以进行对账号的验证
   */
  public function ApplyForParty(Request $request)
  {
    $params = $request->param();
    $this->validate(
      $params,
      [
        'name|名字' => 'require',
        'start_time|开始时间' => 'require',
        'end_time|结束时间' => 'require'
      ]
    );
    $guid = Tool::AutomaticID($params['name']);
    $find = ModelParty::where('party_guid', $guid)->find();
    //!防止重复申请
    if ($find) {
      return Tool::msg(404, "该用户已申请!");
    }
    $ApplyFor = [
      'party_guid' => $guid,
      'party_name' => $params['name'],
      'party_start_time' => $params['start_time'],
      'party_end_time' => $params['end_time'],
    ];
    $create = ModelParty::create($ApplyFor);
    return Tool::msg(200, "申请成功!");

  }
  /**
   * 验证当事人
   * @author  L 
   */
  public function VerifyParty(Request $request)
  {
    $params = $request->param();
    $this->validate(
      $params,
      [
        'name|名字' => 'require',
      ]
    );
    $guid = Tool::AutomaticID($params['name']);
    $find = ModelParty::where('party_guid', $guid)->find();
    if ($find) {

      return Tool::msg(200, "您当前处于当事人!", [], "您可以进行一些特殊操作!");
    } else {
      return Tool::msg(404, "您未处于当事人!", [], "可以进行申请!");
    }
  }
  /**
   * 当事人时间超时
   * @author L
   */
  public function PartyTimeOut(Request $request)
  {
    $params = $request->param();
    $this->validate($params, [
      'name|名字' => 'require',
    ]);

    //*获取时间Data
    $time = Tool::MySQLData();
    //*获取到当前时间的DataTime
    $data_time = Tool::MySQLDataTime();
    //*获取当事人 
    $guid = Tool::AutomaticID($params['name']);
    $find = ModelParty::where('party_guid', $guid)->find();
    if(!$find)return Tool::msg(404,"当前并不是当事人！");
    if ($time >= $find['party_end_time']) {
      $data = [
        'party_delete_time' => $data_time,
      ];
      $find = $find->save($data);
      return Tool::msg(404, "你的当事人已被废除！");
    } else {
      return Tool::msg(200, "当事人身份存在！");

    }
  }
}