<?php

/**
 * 统计数值
 */

namespace app\admin\controller\Statistic;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use app\model\Statistic\Statistic as ModelStatistic;



class Statistic extends BaseController
{
     /**
      * 获取统计数值
      * @author L
      */
     public function getStatistic()
     {
          $Statistic = ModelStatistic::field([
                    'statistic_id',
                    'statistic_title',
                    'statistic_value'
               ])
               ->limit(0, 4) //!限制4条数据
               ->select()->toArray();
          return Tool::msg(200, "获取成功!", $Statistic);
     }
     /**
      * 编辑统计组件
      * @author L
      */
      public function EditStatistic(Request $request)
      {
          $params = $request->param();
          $this->validate($params,[
               'Id|统计组件id'=>'require',
               'title|统计组件标题'=>'require',
               'value|统计组件值'=>'require',
          ]);
          $change = [
               'statistic_title'=>$params['title'],
               'statistic_value'=>$params['value'],
          ];
          $EditStatistic = ModelStatistic::where('statistic_id',$params['Id'])->find()->save($change);
          return Tool::msg(200,"编辑成功!");
      }
}
