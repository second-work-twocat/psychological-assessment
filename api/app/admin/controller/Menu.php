<?php

/**
 * 菜单
 */

namespace app\admin\controller;

use app\BaseController;
use app\admin\controller\Tool as Tool;
use app\Request;
use app\model\Menu as ModelMenu;
use app\model\MenuSecond as ModelMenuSecond;

class Menu extends BaseController
{
     /**
      * 树形菜单列表
      * @author LIULEI
      */
      public function getTreeMenu()
      {
          $menu = Tool::Tree(ModelMenu::class,ModelMenuSecond::class,"menu_guid",[],[]);
          return Tool::Message(200,"获取成功！",$menu);

      }
     /**
      * 获取前端菜单列表
      * @author L
      */
     public function getBeforeMenu()
     {
          $get = ModelMenu::where("apimenu", 1)->field([
               'menu_guid',
               'menu_icon',
               'menu_name',
               'menu_to',
               'menu_type',
               'menu_order',
               'apimenu',
          ])
               ->order("menu_create_time", "asc")
               ->select()->toArray();
          return Tool::msg(200, "获取成功！", $get);
     }
     /**
      * 获取菜单列表->一级菜单
      * @author L
      */
     public function getMenuList_first()
     {
          // *获取菜单
          $con[] = ["menu_type", "=", 0];
          $con[] = ["apimenu", "=", 0];
          $firstMenu = ModelMenu::where($con)
               ->field([
                    'menu_guid',
                    'menu_icon',
                    'menu_name',
                    'menu_to',
                    'menu_type',
                    'menu_order',
                    'apimenu',
               ])
               ->order('menu_create_time', 'asc')
               ->select()->toArray();
          $rtn = Tool::msg(200, "获取成功!", $firstMenu);
          return $rtn;
     }
     /**
      * 获取菜单列表->二级菜单
      * @author L
      */
     public function getMenuList_second()
     {
          //  获取父菜单
          $Menu = ModelMenu::where('menu_type', 1)
               ->order('menu_create_time', 'desc')->select()->toArray();
          return Tool::msg(200, "获取成功!", $Menu);
     }
     /**
      * 获取菜单列表->二级菜单->子
      * @author L
      */
     public function getMenuList_second_son(Request $request)
     {

          $params = $request->param();
          $this->validate($params, [
               'menu_guid|菜单guid' => 'require',
          ]);
          // 获取子菜单
          $con[] = ['menu_guid', '=', $params['menu_guid']];
          $con[] = ['menu_second_delete_time', '=', null];
          $secondMenu_son = ModelMenuSecond::where($con)
               ->field([
                    'menu_second_id',
                    'menu_second_guid',
                    'menu_second_icon',
                    'menu_second_to',
                    'menu_second_name'
               ])
               ->order('menu_second_create_time', 'asc')
               ->select()->toArray();
          return Tool::msg(200, "获取成功!", $secondMenu_son);
     }
     /**
      * 获取全部子菜单
      * @author L
      */
     public function getAllMenuList_second_son()
     {
          $secondMenu_son = ModelMenuSecond::field([
               'menu_second_id',
               'menu_second_guid',
               'menu_second_icon',
               'menu_second_to',
               'menu_second_name',
               'menu_guid',
               'menu_type',
          ])->select()->toArray();
          return Tool::msg(200, "获取成功!", $secondMenu_son);
     }
     /**
      * 删除菜单
      * todo 删除不必要或用不到的菜单
      */
     public function DeleteMenu(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'guid|菜单guid' => 'require',
               'type|菜单类型' => 'require',
          ]);
          if ($params['type'] == 0) { //*删除一级菜单
               $DeleteMenu = ModelMenu::where('menu_guid', $params['guid'])->find()->delete();
               return Tool::msg(200, "删除成功!");
          }
          if ($params['type'] == 1) { //删除二级菜单
               $DeleteMenu = ModelMenu::where('menu_guid', $params['guid'])->find()->delete();
               //删除二级菜单子级
               $DeleteMenu_son = ModelMenuSecond::where('menu_guid', $params['guid'])->select()->delete();
               return Tool::msg(200, "删除成功！");
          }
          if ($params['type'] == 3) {
               // 删除子菜单
               $DeleteMenu = ModelMenuSecond::where('menu_second_guid', $params['guid'])->find()->delete();
               return Tool::msg(200, "删除成功!");
          }
     }
     /**
      * 添加子菜单
      * @author L
      */
     public function AddMenu_second_son(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'to|路由跳转' => 'require',
               'name|子菜单名字' => 'require',
               'menu_guid|菜单guid' => 'require',
          ]);
          $guid = Tool::AutomaticID($params['name']);
          $add = [
               'menu_second_guid' => $guid,
               'menu_second_icon' => 'Home',
               'menu_second_to' => $params['to'],
               'menu_second_name' => $params['name'],
               'menu_guid' => $params['menu_guid']
          ];
          $AddMenu_second_son = ModelMenuSecond::create($add)->toArray();
          return Tool::msg(200, "添加成功!", $AddMenu_second_son);
     }
     /**
      * 添加菜单
      * @author L
      */
     public function AddMenu(Request $request)
     {
          $params = $request->param();
          $this->validate($params, [
               'type|菜单类型' => 'require',
               'to|路由跳转' => 'require',
               'name|菜单名字' => 'require',
               'api|成为Api菜单' => "require",
          ]);
          $guid = Tool::AutomaticID($params['name']);
          $add = [
               'menu_guid' => $guid,
               'menu_icon' => 'House',
               'menu_type' => $params['type'],
               'menu_to' => $params['to'],
               'menu_name' => $params['name'],
               'apimenu' => $params['api'],
          ];
          $AddMenu = ModelMenu::create($add)->toArray();
          return Tool::msg(200, "添加成功!", $AddMenu);
     }
}