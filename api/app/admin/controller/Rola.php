<?php
/**
 * 用户职位
 */
namespace app\admin\controller;

use app\BaseController;
use app\model\Rola as ModelRola;
use app\admin\controller\Tool as Tool;
use app\Request;

class Rola extends BaseController
{
     /**
      * 新建Rola
      * @author L
      */
     public function CreateRola()
     {
          $content = "清洁工";
          $content_guid = Tool::AutomaticID($content);
          $data = [
               'rola_guid' => $content_guid,
               'rola_content' => $content,
          ];
          $Rola = ModelRola::create($data)->toArray();
          $rtn = Tool::msg(200, "创建成功!", $Rola);
          return $rtn;
     }
     /**
      * 获取单条用户职位
      * @author L
      */
      public function getRola_one(Request $request)
      {
          $params = $request->param();
          $this->validate($params,[
               'rola_guid|职位guid'=>'require',
          ]);
          $getRola = ModelRola::where('rola_guid',$params['rola_guid'])->select()->toArray();
          return Tool::msg(200,"获取成功!",$getRola);
      }
}
