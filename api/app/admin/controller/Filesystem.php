<?php
/**
 * 文件云端
 */
namespace app\admin\controller;

use app\BaseController;
use app\Request;
use app\model\Filesystem as ModelFilesystem;

class Filesystem extends BaseController
{
        /**
         * 获取文件云端路径
         */
        private function getURL()
        {
                //获取路径
                $url = __DIR__;
                $url = str_replace("app\admin\controller", "public\Cloud", $url);
                return $url;
        }
        /**
         * 文件云端上传
         */
        public function FileCLoudUpload(Request $request)
        {
                /**
                 * 接受需求参数
                 * @author L
                 */
                $params = $request->param();
                $this->validate($params, [
                        'user|用户名字' => 'require',
                        'name|名字' => 'require',
                        // 'type|类型'=>'require',
                        'size|大小' => 'require',
                        'file|文件' => 'require',
                ]);
                //获取路径
                $url = __DIR__;
                $url = str_replace("app\admin\controller", "public\Cloud\\" . $params['user'] . "/", $url);
                $file_data = $params['file'];
                //图片上传
                if ($params['type'] == "image/jpeg" || $params['type'] == "image/png") {
                        //*如果传入的file是图片，那么会经过base64编码
                        $file = base64_decode($file_data); //todo 解析
                        //*上传图片
                        $Upload = Tool::AutomaticFile($url, $params['name'], "", $file);
                }
                //文件上传
                else if ($params["type"] == "text/html") {
                        $file = base64_decode($file_data); //todo 解析
                        // $file = file_get_contents($file_data);
                        //*上传文件
                        $Upload = Tool::AutomaticFile($url, $params['name'], "", $file);
                } else {
                        $file = base64_decode($file_data); //todo 解析
                        // $file = file_get_contents($file_data);
                        //*上传文件
                        $Upload = Tool::AutomaticFile($url, $params['name'], "", $file);
                }
                return Tool::msg(200, "上传成功!");
        }
        /**
         * 获取当前用户的文件云端文件列表
         * @author LIULEI
         * !如果没有则错误。
         */
        public function getUserFileList(Request $request)
        {

                (new Tool())->BatchVerification($request->param(), [
                        "user/用户名字/require"
                ]);
                $params = $request->param();
                $find = ModelFilesystem::where("filesystem_user", $params["user"])->find();
                if (!$find)
                        return Tool::Message(404, "当前用户未创建文件云端!");
                $url = $this->getURL();
                $filelist = glob($url . "\\" . $params["user"] . "\\*");
                //*去除文件前缀
                for ($i = 0; $i < count($filelist); $i++) {
                        $filelist[$i] = str_replace($url . "\\" . $params["user"] . "\\", "", $filelist[$i]);
                }
                //*获取所有文件列表
                return Tool::Message(200, "获取成功!", ["file" => $filelist, "path" => $url . "\\" . $params["user"] . "\\"]);

        }
        /**
         * 创建当前用户的文件云端
         */
        public function CreateUserFileCloud(Request $request)
        {
                (new Tool())->BatchVerification($request->param(), [
                        "user/用户名字/require"
                ]);
                $params = $request->param();
                $data = [
                        "filesystem_user" => $params["user"]
                ];
                //?判断是否创建文件云端。
                $find = ModelFilesystem::where("filesystem_user", $params["user"])->find();
                if ($find)
                        return Tool::Message(404, "当前用户已创建文件云端!");
                //获取路径
                $url = $this->getURL();
                //创建云端文件夹
                Tool::AutomaticOs($url, $params["user"]);
                ModelFilesystem::create($data);
                return Tool::Message(200, "创建成功!");

        }
        /**
         * 删除当前用户的文件云端
         */
        public function DeleteUserFileCloud(Request $request)
        {
                (new Tool())->BatchVerification($request->param(), [
                        "user/用户名字/require"
                ]);
                $params = $request->param();
                $data = [
                        "filesystem_user" => $params["user"]
                ];
                //?判断是否创建文件云端。
                $find = ModelFilesystem::where("filesystem_user", $params["user"])->find();
                if (!$find)
                        return Tool::Message(404, "当前用户未创建文件云端!");
                //获取路径
                $url = $this->getURL();
                //删除云端文件夹
                $filelist = glob($url . "\\" . $params["user"] . "\\*");
                //删除文件
                for ($i = 0; $i < count($filelist); $i++) {
                        Tool::DeleteFile($filelist[$i]);
                }
                //删除文件夹
                Tool::DeleteDir($url . "\\" . $params["user"]);
                $find->delete();
                return Tool::Message(200, "文件云端删除完毕!");

        }

}