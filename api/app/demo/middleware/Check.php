<?php
declare (strict_types = 1);

namespace app\demo\middleware;

class Check
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //绕过请求
        return $next($request);
    }
}
