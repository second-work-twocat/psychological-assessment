<?php

namespace app\model\Dictionary;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
class Dictionary extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'dictionary_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'dictionary_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'dictionary_id' => 'int',
          'dictionary_guid' => 'string',
          'dictionary_name'=>'string',
          'dictionary_value'=>'string',
          'dictionary_status'=>'int',
          'dictionary_create_time' => 'datetime',
          'dictionary_update_time' => 'datetime',
          'dictionary_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'dictionary_create_time';
     // 修改时间
     protected $updateTime = 'dictionary_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
     
}
