<?php


namespace app\model;

use think\Model;
use think\model\concern\SoftDelete;


class Makecode extends Model
{

        //开启软删除
        use SoftDelete;
        // 删除字段
        protected $deleteTime = 'makecode_delete_time';
        // 设置json类型字段
        protected $json = [];
        // 设置主键名
        protected $pk = 'makecode_id';
        // 设置废弃字段
        protected $disuse = [];
        // 设置字段信息
        protected $schema = [
                'makecode_id' => 'int',
                'makecode_guid' => 'string',
                'makecode_name' => 'string',
                'makecode_user' => 'string',
                'makecode_url'=>'string',
                'makecode_type'=>"int",
                'makecode_create_time' => 'datetime',
                'makecode_update_time' => 'datetime',
                'makecode_delete_time' => 'datetime',
        ];
        // 开启自动写入时间戳字段
        protected $autoWriteTimestamp = 'datetime';
        // 创建时间
        protected $createTime = 'makecode_create_time';
        // 修改时间
        protected $updateTime = 'makecode_update_time';
        /**
         * 新增前
         */
        public static function onBeforeInsert(self $model): void
        {
        }

        /**
         * 更新前
         */
        public static function onBeforeUpdate(self $model): void
        {
        }

        /**
         * 删除前
         */
        public static function onBeforeDelete(self $model): void
        {
        }
        //修改生成代码类型
        public function getMakecodeTypeAttr($name,$object)
        {
                $arr = [1=>"前台",2=>"前端"];
                return $arr[$name];
        }

}