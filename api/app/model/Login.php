<?php

namespace app\model;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
class Login extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'login_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'login_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'login_id' => 'int',
          'login_guid' => 'string',
          'login_user' => 'string',
          'login_password' => 'string',
          'login_rola_guid' => 'string',
          'login_start' =>'int',
          'login_create_time' => 'datetime',
          'login_update_time' => 'datetime',
          'login_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'login_create_time';
     // 修改时间
     protected $updateTime = 'login_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
     /**
      * 事物关联
      */
     public function userRola()
     {

          return $this->hasOne(Rola::class, 'rola_guid', 'login_rola_guid');
     }
     /**
      * 修改器
      */
      public function getLoginStartAttr($value)
      {
          $arr = array("0"=>"未验证","1"=>"已验证");
          return $arr[$value];
      }
     
}
