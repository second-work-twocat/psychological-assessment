<?php

namespace app\model;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;

class User extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'user_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'user_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'user_id' => 'int',
          'user_guid' => 'string',
          'user_name' => 'string',
          'user_img'=>'string',
          'user_information'=>'string',
          'login_guid'=>'string',
          'user_create_time' => 'datetime',
          'user_update_time' => 'datetime',
          'user_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'user_create_time';
     // 修改时间
     protected $updateTime = 'user_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
}