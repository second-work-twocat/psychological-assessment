<?php
/**
 * 生成代码类型\
 * 生成代码-L
 */
namespace app\model\Makecodetype;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
use app\admin\controller\Tool as Tool;
class Makecodetype extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'makecodetype_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'makecodetype_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'makecodetype_id' => 'int',
          'makecodetype_guid' => 'string',
          'makecodetype_name'	=>	'varchar',
'makecodetype_suffix'	=>	'varchar',

          'makecodetype_create_time' => 'datetime',
          'makecodetype_update_time' => 'datetime',
          'makecodetype_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'makecodetype_create_time';
     // 修改时间
     protected $updateTime = 'makecodetype_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
     
 
}
