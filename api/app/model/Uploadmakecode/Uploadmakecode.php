<?php
/**
 * 上传生成代码\
 * 生成代码-L
 */
namespace app\model\Uploadmakecode;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
use app\admin\controller\Tool as Tool;

class Uploadmakecode extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'uploadmakecode_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'uploadmakecode_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'uploadmakecode_id' => 'int',
          'uploadmakecode_guid' => 'string',
          'uploadmakecode_name' => 'varchar',
          'uploadmakecode_img' => 'varchar',
          'uploadmakecode_type' => 'varchar',
          'uploadmakecode_create_time' => 'datetime',
          'uploadmakecode_update_time' => 'datetime',
          'uploadmakecode_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'uploadmakecode_create_time';
     // 修改时间
     protected $updateTime = 'uploadmakecode_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
     //*修改上传生成代码类型
     // public function getUploadmakecodeTypeAttr($name,$object)
     // {
     //      $arr =[1=>'HTML',2=>'Vue'];
     //      return $arr[$name];
     // }

}