<?php

namespace app\model;

use app\model\MenuSecond;
use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
class Token extends ThinkModel
{
     //开启软删除
     // use SoftDelete;
     // 删除字段
     // protected $deleteTime = 'token_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'token_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'token_id' => 'int',
          'token_guid' => 'string',
          'token_user'=>'string',
          'token_value' => 'string',
          'token_expire_time' => 'datetime',
          'token_create_time' => 'datetime',
          'token_update_time' => 'datetime',
          'token_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'token_create_time';
     // 修改时间
     protected $updateTime = 'token_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
}
