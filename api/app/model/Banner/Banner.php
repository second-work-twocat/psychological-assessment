<?php

namespace app\model\Banner;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
class Banner extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'banner_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'banner_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'banner_id' => 'int',
          'banner_guid' => 'string',
          'banner_src' => 'string',
          'banner_position' => 'int',
          'banner_create_time' => 'datetime',
          'banner_update_time' => 'datetime',
          'banner_delete_time' => 'datetime',
          'dictionary_guid'=>'string',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'banner_create_time';
     // 修改时间
     protected $updateTime = 'banner_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
     
}
