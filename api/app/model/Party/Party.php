<?php

/**
 * 当事人
 */

namespace app\model\Party;

use think\Model as ThinkModel;
use think\model\concern\SoftDelete;
use app\admin\controller\Tool as Tool;

class Party extends ThinkModel
{
     //开启软删除
     use SoftDelete;
     // 删除字段
     protected $deleteTime = 'party_delete_time';
     // 设置json类型字段
     protected $json = [];
     // 设置主键名
     protected $pk = 'party_id';
     // 设置废弃字段
     protected $disuse = [];
     // 设置字段信息
     protected $schema = [
          'party_id' => 'int',
          'party_guid' => 'string',
          'party_name' => 'string',
          'party_start_time' => 'date',
          'party_end_time' => 'date',
          'party_create_time' => 'datetime',
          'party_update_time' => 'datetime',
          'party_delete_time' => 'datetime',
     ];
     // 开启自动写入时间戳字段
     protected $autoWriteTimestamp = 'datetime';
     // 创建时间
     protected $createTime = 'party_create_time';
     // 修改时间
     protected $updateTime = 'party_update_time';
     /**
      * 新增前
      */
     public static function onBeforeInsert(self $model): void
     {
     }

     /**
      * 更新前
      */
     public static function onBeforeUpdate(self $model): void
     {
     }

     /**
      * 删除前
      */
     public static function onBeforeDelete(self $model): void
     {
     }
}