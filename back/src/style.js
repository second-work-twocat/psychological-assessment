import axios from "axios";
import { ElMessage } from 'element-plus'
import Cookie from "js-cookie";

/**
 * Cookie过期时间(一天)
 */
var cookie_time = 1;
//Token值
export var tokenValue = Cookie.get("UUID");
/**
 * 刷新
 * @author L
 */
export function Load() {
     window.location.reload(true)
}
/**
 * 随机数
 */
export function RandomNumber(min, max) {
     return Math.floor(Math.random() * (max - min) + min)
}
/**
 * 清空本地保存
 */
export function Clear() {
     localStorage.clear()
}
/**
 * 请求文件上传
 * @author L
 */
export async function Upload(data) {
     return await axios.post('/api/Upload/Upload', data, { "Content-Type": "multipart/form-data" })
          .then((res) => {
               if (res.data.code == 200) {
                    ElMessage({
                         message: res.data.msg,
                         type: 'success',
                    })
               } else {
                    ElMessage({
                         message: res.data.msg,
                         type: 'error',
                    })
               }
          })
}
/**
 * 取消文件上传
 * @author L
 */
export async function CloseUpload(data) {
     return await axios.post('/api/Upload/CloseUpload', data, { "Content-Type": "multipart/form-data" })
          .then((res) => {
               if (res.data.code == 200) {
                    ElMessage({
                         message: res.data.msg,
                         type: 'success',
                    })
               } else {
                    ElMessage({
                         message: res.data.msg,
                         type: 'error',
                    })
               }
          })
}
/**
 * 报错自拟
 * @author L
 */
export function Vipe_Error(data) {
     console.error(data);
}
/** 
 *获取图片转base64
 *@author L
*/
export function getBase64(file) {
     return new Promise(function (resolve, reject) {
          const reader = new FileReader()
          let imgResult = ""
          reader.readAsDataURL(file)//*读取文件内容
          reader.onload = function () {
               imgResult = reader.result //*获取base64内容
          }
          //*转换失败
          reader.onerror = function (error) {
               reject(error)
          }
          //*转换结束
          reader.onloadend = function () {
               resolve(imgResult)
          }
     })
}
/**
 * 读取上传文件
 * @author L
 */
export function getFileText(file) {
     return new Promise(function (resolve, reject) {
          const reader = new FileReader()
          let Text = ""
          reader.readAsText(file)
          reader.onload = function () {
               Text = reader.Text
          }
     })
}
/**
 * Cookie设置->登录超时
 * @author L
 * @param token->Token值
 */
export function SetCookie(token) {
      Cookie.set("UUID", token, { expires: cookie_time });
}