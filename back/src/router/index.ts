import { createRouter, createWebHistory } from "vue-router";
import routes from '~pages';


// 默认重定向
routes.push({
  path: '/',
  redirect: { name: 'Home' },
})

const router = createRouter({
  history: createWebHistory(),
  routes,
});


export default router;
