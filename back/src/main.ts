import { createApp, reactive, ref } from 'vue'
import './style.css'
import { Clear, Load } from './style.js'
import { createRouter, createWebHistory } from 'vue-router';
import App from './App.vue'
import Index from './views/Manage.vue'
import routes from 'virtual:generated-pages'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { TokenValue, TokenVerifyTime } from './service/Token.js'
import { ElMessage, ElMessageBox } from 'element-plus'
import type { Action } from 'element-plus'
import nprogress from 'nprogress';
import 'nprogress/nprogress.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { ElLoading } from 'element-plus'
import Cookie from "js-cookie";



//*获取当前路径
//todo 如果在登录页的时候，那么则不会有提示框出现!
let currentPath = location.pathname;


/**
 * 刷新进度条
 */
nprogress.start();
nprogress.done();

routes.push({
  path: '/',
  redirect: { name: 'Home' },
});
const router = createRouter({
  history: createWebHistory(),
  routes,
});

const app = createApp(App);
const appindex = createApp(Index)


//*全局刷新
const fullscreenLoading = ref(false)
if (currentPath != "/login") //*登录页不进行刷新
{
  const loading = ElLoading.service({
    lock: true,
    text: '刷新中...',
    background: 'rgba(0, 0, 0, 0.8)',
  })
  setTimeout(() => {
    loading.close()
  }, 800);
}
// ?全局配置
//*五分钟刷新一次
setTimeout(() => {
  Load()
}, 3000000)
//! 如果浏览3个小时的话,强行下线(如需要.请自调)
setTimeout(() => {
  Clear()
  Load()
}, 10800000);
//10800000
setTimeout(() => {
  //!如果本地没有User，那么则退回到登录页-进行登录（登录超时）
  if (localStorage.getItem("User") == null && currentPath != '/login') {
    ElMessageBox.alert('请重新登录!', '登录超时', {
      confirmButtonText: 'Back',
      callback: (action: Action) => {
        location.href = "/login" //*返回登录页
      },
    })
  }
  const data = { User: localStorage.getItem('User') };
  if (currentPath != "/login") {
    //?判断是否有Token存在,如果没有则需要返回登录去新建一条Token
    TokenVerifyTime(data).then((res) => {
      if (res.code == 404 && currentPath != '/login') {
        ElMessageBox.alert('Token已过期或不存在!', '验证:', {
          confirmButtonText: 'Back',
          callback: (action: Action) => {
            location.href = "/login" //*返回登录页
            Clear()//*清空本地储存
          },

        })
      }
      if (res.code == 200) {
        // *拿取Cookie->Token值
        const token = Cookie.get("UUID")
        // !如果没有本地Token值，则退回
        // *执行判断Token时间是否超时->如果则提示登录超时则退回
        // if (token == null && currentPath != '/login') {
        //   // location.href =  "/login" 

        // }
        //*本地有Token值，那么则进行Token的验证 -> 验证成功则进行对接口的请求，否则验证失败!
        if (token == null && currentPath != "/login") {
          ElMessageBox.alert('(Cookie)登录超时！', '验证:', {
            confirmButtonText: 'Back',
            callback: (action: Action) => {
              location.href = "/login" //*返回登录页
              Clear()//*清空本地储存
              Cookie.remove("UUID")//*清除Token
            },
          })
        }
      }
    })
  }
}, 500);

// 注册Icon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}


app.use(ElementPlus)
app.use(router);


app.mount('#app');
