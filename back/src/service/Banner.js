import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取轮播图列表->后台管理系统
 */
export async function getBannerList_admin(data) {
     return await axios.get('/api/Banner.Banner/getBannerList_admin', data).then((res) => {
          return res.data
     })
}
/**
 * 编辑轮播图
 */
export async function EditBanner(data) {
     return await axios.post('/api/Banner.Banner/EditBanner', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}
/**
 * 添加轮播图->后台管理系统
 */
export async function AddBanner(data) {
     return await axios.post('/api/Banner.Banner/AddBanner', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}
/**
 * 删除轮播图
 */
export async function DeleteBanner(data) {
     return await axios.post('/api/Banner.Banner/DeleteBanner', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}