import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 创造Token
 * @author L
 */
export async function CreateToken(data) {
    return await axios.post('/api/Token/CreateToken', data).then((res) => {
        return res.data
    })
}
/**
 * 验证Token
 * @author L
 */
export async function TokenVerify(data) {
    return await axios.post('/api/Token/TokenVerify', data).then((res) => {
        return res.data
    })

}
/**
*  Token验证约期
* @auther L
*/
export async function TokenVerifyTime(data) {
    return await axios.post('/api/Token/TokenVerifyTime',data).then((res) => {
        return res.data
    })
}
/**
 * Token拿值判断
 * @author L
 */
export async function TokenValue(data) {
    return await axios.post('/api/Token/TokenValue', data).then((res) => {
        return res.data
    })
}
