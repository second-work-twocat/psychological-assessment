import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

export async function MakeCode(data)
{
     return await axios.post('/api/Root/MakeCode',data).then((res)=>
     {
          return res.data
     })
}
/**
 * 生成前端登录页面
 */
export async function MakeBeforePage_Login(data)
{
     return await axios.post('/api/Root/MakeBeforePage_Login',data).then((res)=>
     {
          return res.data
     })
}
/**
 * 获取生成代码列表
 * @author L
 */
export async function getMakeCodeList(data)
{
     return await axios.get('/api/MakeCode/getMakeCodeList',data).then((res)=>
     {
          return res.data
     })
}

/**
 * 删除区块
 * @author L
 */
export async function DeleteBlock(data)
{
     return await axios.post('/api/Root/DeleteBlock',data).then((res)=>
     {
          return res.data
     })
}
/**
 * 生成前端Api接口
 * @author L
 */
export async function MakeApi(data)
{
     return await axios.post('/api/Root/MakeApi',data).then((res)=>
     {
          return res.data
     })
}

/**
 * 生成前端其他页面
 */
export async function MakeBeforePage_OverPage(data)
{
     return await axios.post('/api/Root/MakeBeforePage_OverPage',data).then((res)=>
     {
          return res.data
     })
}