/**
 * 生成代码-L
 */
import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";


/**
 * 获取生成代码类型列表
 */
export async function getMakecodetypeList(data) {
     return await axios.get('/api/Makecodetype.Makecodetype/getMakecodetypeList', data).then((res) => {
          return res.data
     })
}
/**
 * 搜索生成代码类型
 */
export async function SearchMakecodetype(data) {
     return await axios.post('/api/Makecodetype.Makecodetype/SearchMakecodetype', data).then((res) => {
          return res.data
     })
}
/**
 * 批量删除生成代码类型
 * @author L
 */
export async function BatchDeleteMakecodetype(data) {
     return await axios.post("/api/Makecodetype.Makecodetype/BatchDeleteMakecodetype", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}
/**
 * 获取生成代码类型历史记录
 * @author L
 */
export async function getTrasheMakecodetype(data) {
     return await axios.get('/api/Makecodetype.Makecodetype/getTrasheMakecodetype', data).then((res) => {
          return res.data
     })
}


/**
 * 恢复生成代码类型历史记录
 * @author L
 */
export async function RestoreTrasheMakecodetype(data) {
     return await axios.post("/api/Makecodetype.Makecodetype/RestoreTrasheMakecodetype", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}
/**
 * 彻底删除生成代码类型历史记录
 * @author L
 */
export async function ForceTrasheMakecodetype(data) {
     return await axios.post("/api/Makecodetype.Makecodetype/ForceTrasheMakecodetype", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}



               /**
                * 添加生成代码类型
                * @author L
                */
                export async function AddMakecodetype(data)
                {
                    return await axios.post("/api/Makecodetype.Makecodetype/AddMakecodetype",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               
               /**
                * 编辑生成代码类型
                * @author L
                */
                export async function EditMakecodetype(data)
                {
                    return await axios.post("/api/Makecodetype.Makecodetype/EditMakecodetype",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               
               /**
                * 删除生成代码类型
                * @author L
                */
                export async function DeleteMakecodetype(data)
                {
                    return await axios.post("/api/Makecodetype.Makecodetype/DeleteMakecodetype",data).then((res) => {
                         if (res.data.code == 200) {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "success",
                              })
                              setTimeout(() => {
                                   Load()
                              }, 1000);
               
                         } else {
                              ElMessage({
                                   message: res.data.msg,
                                   type: "error",
                              })
                         }
                         return res.data
                    })
                }
               