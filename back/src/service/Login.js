import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load, SetCookie } from "../style";



/**
 * 登录验证
 * @author L
 */
export async function LoginAdmin(data) {
     return await axios.post('/api/Login/Login', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               // *保存账号
               localStorage.setItem('User', res.data.data[0].login_user)
               // *保存密码
               localStorage.setItem('PassWord', res.data.data[0].login_password)
               // *保存用户guid
               localStorage.setItem('Guid', res.data.data[0].login_guid)
               //*保存职位rola
               localStorage.setItem('Rola', res.data.data[0].login_rola_guid)
               //*保存ip
               localStorage.setItem('IP', res.data.IP)
               //*保存Token
               SetCookie(res.data.data[0].token)
               setTimeout(() => {
                    window.location.href = "/home"
                    setTimeout(() => { Load() }, 1000)
               }, 500);
               return res.data
          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
     })
}
/**
 * 获取验证码
 * @author L
 */
export async function getVerification(data) {
     return await axios.get('/api/Verification/getVerification', data).then(res => {
          return res.data
     })
}
/**
 * 验证码判断
 * @author L
 */
export async function Verification_Judgment(data) {
     return axios.post('/api/Verification/Verification_Judgment', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
          return res.data
     })
}
/**
 * 获取用户登录信息
 * @author L
 */
export async function getUser(data) {
     return await axios.post('/api/Login/getUser', data).then((res) => {
          return res.data
     })
}
/**
 * 获取账号列表
 * @author L
 */
export async function getLoginList(data) {
     return await axios.post('/api/Login/getLoginList', data).then((res) => {
          return res.data
     })
}
/**
 * 获取未验证账号列表
 * @author L
 */
export async function getNotVerifiedLogin(data) {
     return await axios.post('/api/Login/getNotVerifiedLogin', data).then((res) => {
          return res.data
     })
}
/**
 * 新建账号
 * @author L
 */
export async function CreateLogin(data) {
     return await axios.post('/api/Login/CreateLogin', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
          return res.data
     })
}
/**
 * 删除账号
 * @author L
 */
export async function DeleteLogin(data) {
     return await axios.post('/api/Login/DeleteLogin', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
          return res.data
     })
}
/**
 * 验证账号
 * @author L
 */
export async function VerifiedLogin(data) {
     return await axios.post('/api/Login/VerifiedLogin', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
          return res.data
     })
}