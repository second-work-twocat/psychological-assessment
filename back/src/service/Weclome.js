import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";
import { ElNotification } from 'element-plus'

/**
 * 随机获取欢迎语
 * @author L
 */
export async function getWeclome(data) {
     return await axios.get('/api/Weclome/getWeclomeList', data).then((res) => {
          if(res.data.code == 200)
          {
               ElNotification({
                    title: '欢迎回来!',
                    message: res.data.data[0].weclome_content,
                    duration: 0,
                  })
          }
     })
}
