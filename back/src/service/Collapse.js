import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取折叠面板
 * @author L
*/
export async function getCollapse(data) {
     return await axios.get('/api/Collapse.Collapse/getCollapse', data).then((res) => {
          return res.data
     })
}
/**
 * 编辑折叠面板
 * @author L
 */
export async function EditCollapse(data) {
     return await axios.post('/api/Collapse.Collapse/EditCollapse', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}
