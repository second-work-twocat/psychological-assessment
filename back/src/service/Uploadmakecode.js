/**
 * 生成代码-L
 */
import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取单条
 */
 export async function getItemUploadmakecode(data) {
     return await axios.post('/api/Uploadmakecode.Uploadmakecode/getItemUploadmakecode', data).then((res) => {
          return res.data
     })
}
/**
 * 获取上传生成代码列表
 */
export async function getUploadmakecodeList(data) {
     return await axios.get('/api/Uploadmakecode.Uploadmakecode/getUploadmakecodeList', data).then((res) => {
          return res.data
     })
}
/**
 * 搜索上传生成代码
 */
export async function SearchUploadmakecode(data) {
     return await axios.post('/api/Uploadmakecode.Uploadmakecode/SearchUploadmakecode', data).then((res) => {
          return res.data
     })
}
/**
 * 批量删除上传生成代码
 * @author L
 */
export async function BatchDeleteUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/BatchDeleteUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}
/**
 * 获取上传生成代码历史记录
 * @author L
 */
export async function getTrasheUploadmakecode(data) {
     return await axios.get('/api/Uploadmakecode.Uploadmakecode/getTrasheUploadmakecode', data).then((res) => {
          return res.data
     })
}


/**
 * 恢复上传生成代码历史记录
 * @author L
 */
export async function RestoreTrasheUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/RestoreTrasheUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}
/**
 * 彻底删除上传生成代码历史记录
 * @author L
 */
export async function ForceTrasheUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/ForceTrasheUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}



/**
 * 添加上传生成代码
 * @author L
 */
export async function AddUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/AddUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}

/**
 * 编辑上传生成代码
 * @author L
 */
export async function EditUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/EditUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}

/**
 * 删除上传生成代码
 * @author L
 */
export async function DeleteUploadmakecode(data) {
     return await axios.post("/api/Uploadmakecode.Uploadmakecode/DeleteUploadmakecode", data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: "success",
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: "error",
               })
          }
          return res.data
     })
}
