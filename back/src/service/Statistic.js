import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取统计组件
 * @author L
 */
export async function getStatistic(data) {
     return await axios.get('/api/Statistic.Statistic/getStatistic', data).then((res) => {
          return res.data
     })
}
/**
 * 编辑统计组件
 */
export async function EditStatistic(data){
     return await axios.post('/api/Statistic.Statistic/EditStatistic',data).then((res)=>{
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}