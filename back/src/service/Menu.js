import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";


/**
 * 获取树形菜单
 * @author L
 */
export async function getTreeMenu(data) {
     return await axios.get('/api/Menu/getTreeMenu', data).then((res) => {
          return res.data
     })
}

/**
 * 获取菜单列表->一级菜单
 * @author L
 */
export async function getMenuList_first(data) {
     return await axios.get('/api/Menu/getMenuList_first', data).then((res) => {
          return res.data
     })
}
/**
 * 获取菜单列表->二级菜单
 * @author L
 */
export async function getMenuList_second(data) {
     return await axios.get('/api/Menu/getMenuList_second', data).then((res) => {
          return res.data
     })
}
/**
 * 获取菜单列表->二级菜单->子
 * @author L
 */
export async function getMenuList_second_son(data) {
     return await axios.post('/api/Menu/getMenuList_second_son', data).then((res) => {
          return res.data
     })
}
/**
 * 获取全部子菜单
 */
export async function getAllMenuList_second_son(data){
     return await axios.get('/api/Menu/getAllMenuList_second_son',data).then((res)=>{
          return res.data
     })
}
/**
 * 删除菜单
 *@author L
 */
export async function DeleteMenu(data)
{
     return await axios.post('/api/Menu/DeleteMenu',data).then((res)=>{
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}
/**
 * 添加子菜单
 * @author L
 */
export async function AddMenu_second_son(data)
{
     return await axios.post('/api/Menu/AddMenu_second_son',data).then((res)=>{
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}
/**
 * 添加菜单
 * @author L
 */
export async function AddMenu(data)
{
     return await axios.post('/api/Menu/AddMenu',data).then((res)=>{
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               setTimeout(() => {
                    Load()
               }, 1000);

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })
          }
          return res.data
     })
}