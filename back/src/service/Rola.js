import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取单条用户职位
 * @author L
 */
export async function getRola_one(data)
{
     return axios.post('/api/Rola/getRola_one',data).then((res)=>
     {
          return res.data
     })
}