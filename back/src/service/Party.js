import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 申请当事人
 * @author L
 */
export async function ApplyForParty(data) {
    return await axios.post('/api/Party.Party/ApplyForParty',data).then((res) => {
        if (res.data.code == 200) {
            ElMessage({
                message: res.data.msg,
                type: 'success',
            })
            setTimeout(() => {
                Load()
            }, 1000);

        } else {
            ElMessage({
                message: res.data.msg,
                type: 'error',
            })
        }
        return res.data
    })
}
/**
 * 验证当事人
 * @author L
 */
export async function VerifyParty(data) 
{
    return await axios.post('/api/Party.Party/VerifyParty',data).then((res) => {
        return res.data
    })
}
/**
 * 当事人时间超时
 * @author L
 */
export async function PartyTimeOut(data)
{
    return await axios.post('/api/Party.Party/PartyTimeOut',data).then((res)=>{
        return res.data
    })
}