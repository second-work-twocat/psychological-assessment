import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 编辑用户
 */
export async function EditUser(data) {
     return await axios.post('/api/Login/EditUser', data).then((res) => {
          if (res.data.code == 200) {
               ElMessage({
                    message: res.data.msg,
                    type: 'success',
               })
               // *编辑完成后重新登录
               window.location.href = "/login"
      

          } else {
               ElMessage({
                    message: res.data.msg,
                    type: 'error',
               })

          }
          return res.data
     })
}
