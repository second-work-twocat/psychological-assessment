import axios from "axios";
import { ElMessage } from 'element-plus'
import { useRouter } from "vue-router";
import router from '../router';
import { Load } from "../style";

/**
 * 获取当前用户的文件云端文件列表
 * @author L
*/
export async function getUserFileList(data) {
     return await axios.post('/api/Filesystem/getUserFileList', data).then((res) => {
          return res.data
     })
}
/**
 * 创建当前用户的文件云端
 * @author L
*/
export async function CreateUserFileCloud(data) {
     return await axios.post('/api/Filesystem/CreateUserFileCloud', data).then((res) => {
        if (res.data.code == 200) {
                ElMessage({
                     message: res.data.msg,
                     type: 'success',
                })
                Load()
 
           } else {
                ElMessage({
                     message: res.data.msg,
                     type: 'error',
                })
                Load()
           }
           return res.data
     })
}
/**
 * 删除当前用户的文件云端
 * @author L
*/
export async function DeleteUserFileCloud(data) {
     return await axios.post('/api/Filesystem/DeleteUserFileCloud', data).then((res) => {
        if (res.data.code == 200) {
                ElMessage({
                     message: res.data.msg,
                     type: 'success',
                })
                Load()
           } else {
                ElMessage({
                     message: res.data.msg,
                     type: 'error',
                })
                Load()
           }
           return res.data
     })
}
/**
 * 文件云端上传
 */
export async function FileCLoudUpload(data) {
     return await axios.post('/api/Filesystem/FileCLoudUpload', data).then((res) => {
        if (res.data.code == 200) {
                ElMessage({
                     message: res.data.msg,
                     type: 'success',
                })
                Load()
           } else {
                ElMessage({
                     message: res.data.msg,
                     type: 'error',
                })
                Load()
           }
           return res.data
     })
}