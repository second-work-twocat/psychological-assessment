import { defineConfig } from 'vite'
import vue from "@vitejs/plugin-vue";
import Pages from "vite-plugin-pages";
import "./src/style.js"
import Cookie from "js-cookie";


// 127.0.0.1 -> localhost
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Pages({
      dirs: 'src/views',
      exclude: ['**/components/*.vue'],
    }),
  ],
  server: {
    host: 'localhost',
    port: 5173,
    proxy: {
      '/api': {
        target: 'http://localhost:80/Viper/api/public/admin.php',
        headers:{"token": "117s3qp1621opopop2o8q5r3o0807373!@#$%^&*;~`/*-ZwNlAP0jZF0kAFNkAQbjZGbmAD==!@#$%^&*;~`/*-2024-01-15+14%3N01%3N35!@#$%^&*;~`/*-6205116478rpro5n4q380107nn5n4s8sp2o108q9"},
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      }
    }
  },
})

